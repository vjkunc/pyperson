# PyPerson

PyPerson is an open source project that provides a platform for creating and filling out surveys. It can be deployed using Docker Compose, which sets up the runtime environment. The database used is MongoDB.

## Getting started

To get started, run the PyPerson service using Docker Compose:

`docker-compose up` 

After running this command, the web application will be available at `http://localhost:3000`.

## User authentication and registration

When you access the web application, you will be prompted to log in. If you do not have an account, you can create one by clicking the "Register" button and following the instructions.

Every profile is registered as a basic user, who can fill out surveys. If you want to create surveys and collect data about users, you need to change the user's permissions in the MongoDB database `http://localhost:8081`. You can do this by setting the value of the "role" field from 2 to 1. Once you've completed this last step, everything should be intuitive, and you can easily create new surveys for other users.

## Under the Hood

The project is built using the following technologies:

-   Docker-compose version `3`
-   Flask (backend)
-   React (frontend)
-   MongoDB (database)

### Backend

The Flask backend is defined as a service in `docker-compose.yml` under the name `api`. It can be accessed at port `5001` and is dependent on the `postgres-db` service.

### Frontend

The React frontend is defined as a service in `docker-compose.yml` under the name `client`. It can be accessed at port `3000` and is linked to the `api` service.

### Adminer

Adminer is a tool used for managing PostgreSQL and is defined as a service in `docker-compose.yml` under the name `adminer`. It can be accessed at port `8080` and is dependent on the `postgres-db` service.

### MongoDB

The MongoDB database is defined as a service in `docker-compose.yml` under the name `mongo-db`. It can be accessed at port `27017` and is dependent on by the `mongo-express` service.

### Mongo-express

Mongo-express is a tool used for managing MongoDB and is defined as a service in `docker-compose.yml` under the name `mongo-express`. It can be accessed at port `8081` and is dependent on the `mongo-db` service.

## Frontend

The frontend is built using the React library and is responsible for providing a seamless user interface for the application. Here are some of the packages used in the frontend section:

-   `react-jsonschema-form`: A package used for generating forms based on a JSON schema. This package provides an easy way to generate dynamic forms based on the schema that we define.
    
-   `papaparse`: A CSV parsing package that allows us to parse CSV data in the frontend. With this package, we can easily parse CSV data and display it in the application.
    
-   `swiper`: A powerful and flexible slider package that provides an easy way to create a responsive slider component.
    
-   `yup`: A package used for schema validation. This package allows us to define the schema for the data we are handling and validate it easily.
    
-   `react-hook-form`: A performant form package that provides an easy way to handle form inputs, validation and submission.
    
-   `react-jsonschema-form-validation`: A package used to validate form data based on JSON schema.
    
-   `axios`: A promise-based HTTP client that provides an easy way to make HTTP requests from the frontend.
    
-   `redux-persist`: A package used for persisting redux store data to local storage.
    
-   `react-router-dom`: A package that provides a way to handle routing in the React application.
    
-   `sass`: A CSS preprocessor that allows us to write CSS in a more structured way.
    
-   `@fortawesome/fontawesome-svg-core`, `@fortawesome/free-solid-svg-icons`, `@fortawesome/react-fontawesome`: Packages used for adding fontawesome icons to the application.
    
-   `@reduxjs/toolkit`, `react-redux`: Packages used for managing state in the application. Redux is a predictable state container that provides an easy way to manage application state.
    
-   `@testing-library/jest-dom`, `@testing-library/react`, `@testing-library/user-event`: Packages used for testing the React components. These packages provide an easy way to write tests for React components.
    
-   `typescript`: A superset of JavaScript that provides static type-checking and allows us to write more robust and error-free code.
    
-   `web-vitals`: A package used for measuring and reporting web vitals metrics. This package provides an easy way to measure the performance of the application.
    

These packages have been carefully selected to provide a robust and scalable frontend architecture that can handle complex user interactions and data visualization.

## Backend

Our backend is built on top of Flask, a lightweight web application framework written in Python. It provides us with the flexibility to handle various HTTP requests with ease. Here are the important packages we used to build our backend:

-   `Flask`: Flask is a micro web framework written in Python that provides us with tools, libraries, and technologies to build a web application. We used Flask to build the core of our backend application.
    
-   `Flask-SQLAlchemy`: SQLAlchemy is an open-source SQL toolkit and Object-Relational Mapping (ORM) library. Flask-SQLAlchemy is a Flask extension that adds SQLAlchemy support to our Flask application. We used it to connect to our database and interact with it.
    
-   `Marshmallow`: Marshmallow is an object serialization/deserialization library for Python. We used it to serialize/deserialize Python objects to/from JSON objects.
    
-   `Marshmallow-SQLAlchemy`: Marshmallow-SQLAlchemy is a library that adds integration between Marshmallow and SQLAlchemy. We used it to generate database schemas from Python classes.
    
-   `Flask-JWT-Extended`: Flask-JWT-Extended is a Flask extension that adds support for JSON Web Tokens (JWTs) to our Flask application. We used it to handle authentication and authorization.
    
-   `Flask-Mail`: Flask-Mail is a Flask extension that provides simple email sending capabilities to our Flask application. We used it to send emails for various purposes like password reset, welcome emails, and more.
    
-   `Click`: Click is a Python package for creating command line interfaces (CLIs) with a simple and intuitive syntax. We used Click to create custom CLI commands for our Flask application.
    
-   `PyMongo`: PyMongo is a Python driver for MongoDB, a popular NoSQL database. We used it to connect to our MongoDB database and interact with it.
    

These packages help us build a robust and scalable backend that can handle various HTTP requests and interact with the database with ease.

## License

PyPerson is released under the MIT license. Feel free to use it for personal or commercial projects. Contributions to the project are always welcome!