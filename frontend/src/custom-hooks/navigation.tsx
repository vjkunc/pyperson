
import { useSelector, useDispatch } from 'react-redux'
import type { RootState } from '../store/store'

export const useRedirectNormalUser = () => {
    const userToken = useSelector((state: RootState) => state.user.token)
    const userRole = useSelector((state: RootState) => state.user.role)
    if( userToken === "" ) return ["/login"]
    if( userRole === 1) return ["/admin"]
    else return []
}

export const useRedirectAdminUser = () => {
    const userToken = useSelector((state: RootState) => state.user.token)
    const userRole = useSelector((state: RootState) => state.user.role)
    if( userToken === "" ) return ["/login"];
    if( userRole  === 2) return ["/"];
    else return []
}

export const useRedirectUser = () => {
    const userToken = useSelector((state: RootState) => state.user.token)
    if( userToken === "" ) return ["/login"];
    else return []
}

export const useRedirectLogoutUser = () => {
    const userToken = useSelector((state: RootState) => state.user.token)
    if( userToken != "" ) return ["/"];
    else return []
}