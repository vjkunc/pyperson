import Header from '../components/main/Header';
import MainContentAdmin from '../components/main/MainContentAdmin';
import Footer from '../components/main/Footer';
import './MainPage.scss';
import { useNavigate } from 'react-router-dom';
import { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux'
import type { RootState } from '../store/store'
import { useRedirectAdminUser } from '../custom-hooks/navigation';

function MainPage() {
    const navigate = useNavigate()
    const [ redirect ] = useRedirectAdminUser()
    useEffect(()=>{
        if(redirect) navigate(redirect);
    },[]);
    return (
        <section className="main">
            <Header></Header>
            <MainContentAdmin></MainContentAdmin>
            <Footer></Footer>
        </section>
    );
}

export default MainPage