import './NewSurveyPage.scss';
import Header from '../components/main/Header';
import Survey from '../components/survey/Survey';
import Footer from '../components/main/Footer';
import { useNavigate } from 'react-router-dom';
import { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux'
import type { RootState } from '../store/store'
import { useRedirectAdminUser } from '../custom-hooks/navigation';

function NewSurvey () {
    const navigate = useNavigate()
    const [ redirect ] = useRedirectAdminUser()
    useEffect(()=>{
        if(redirect) navigate(redirect);
    },[]);
    return (
        <section>
            <Header></Header>
            <Survey></Survey>
            <Footer></Footer>
        </section>
    );
}

export default NewSurvey