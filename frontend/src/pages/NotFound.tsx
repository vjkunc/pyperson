import { useNavigate } from "react-router-dom";

function NotFound() {
    const navigate = useNavigate();
    function handleClick() {
        navigate("/");
    }
  return (
    <section className="flex items-center h-full p-16 bg-white-900 text-gray-400">
	<div className="container flex flex-col items-center justify-center px-5 mx-auto my-8">
		<div className="max-w-md text-center">
			<h2 className="mb-8 font-extrabold text-9xl text-gray-600">
				<span className="sr-only">Error</span>404
			</h2>
			<p className="text-2xl font-semibold md:text-3xl mb-8">Je nám líto, ale tuto stránku se nám nepodařilo najít.</p>
			<a rel="noopener noreferrer" href="#" className="px-8 py-3 font-semibold rounded text-white" onClick={handleClick} style={{background: "#2C6CB8"}}>Zpět na úvodní plochu</a>
		</div>
	</div>
</section>
  )
}

export default NotFound