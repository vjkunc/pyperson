import { useEffect } from 'react';
import { useRedirectLogoutUser } from '../custom-hooks/navigation';
import './NewPassword.scss';
import { Link, useNavigate } from "react-router-dom";
import axios from 'axios';
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import * as yup from 'yup'
import React, { useState } from 'react';
import { Routes, Route, useParams } from 'react-router-dom';

function NewPassword() {

    const { token } = useParams();

    const navigate = useNavigate()
    const [ redirect ] = useRedirectLogoutUser()
    useEffect(()=>{
        if(redirect) navigate(redirect);
    },[]);
    function handleClick() {
        navigate("/");
    }
    type Form = {
        password: string;
        confirmPwd: string;
        apiError: string;
      }
  
      const formSchema = yup.object().shape({
        password: yup.string()
          .required('Toto pole je povinné')
          .min(2, 'Heslo musí být minimálně 2 znaků dlouhé')
          ,
        confirmPwd: yup.string()
          .required('Toto pole je povinné')
          .oneOf([yup.ref('password')], 'Hesla se neshodují')
          ,
      })
  
      const {setError, register, handleSubmit, formState: { errors } } = useForm<Form>({ 
        resolver: yupResolver(formSchema)
      });
  
      const [isSuccessfullySubmitted, setIsSuccessfullySubmitted] = useState(false);
      const [checkbox, setCheckbox] = useState(false);
  
      const onSubmit = (data: Form) => {
        axios.post('/resetPassword',{
          token: token,
          password: data.password,
        })
        .then(res => {
          setIsSuccessfullySubmitted(true);
          setTimeout(() => {
            navigate('/login');
          }, 3000);
        }).catch(err => {
          console.error(err.response);
          if(err.response.data.errorMessage.includes("expired")) {
            setError('apiError', {
              type: "server",
              message: 'Platnost odkazu pro obnovení hesla vypršela!',
            });
          }
          else if (err.response.data.errorMessage.includes("exist")) {
            setError('apiError', {
              type: "server",
              message: 'Odkaz pro obnovení hesla není platný!',
            });
          }
          else {
            setError('apiError', {
              type: "server",
              message: 'Někde nastala chyba zkuste to znovu!',
            });
          }
        })
      }
  return (
    <section className="change-password">
    <h1>Nové heslo</h1>
    <form className="data" onSubmit={handleSubmit(onSubmit)}>
        <div className="w-full">
        <input type="password" placeholder="Zadejte heslo" id="password" {...register('password')}/>
        {errors.password && <p className="absolute mt-1 text-sm text-red-600">{errors.password.message}</p>}
      </div>
      <div className="w-full">
        <input type="password" placeholder="Ověření hesla" id="confirmPassword" {...register('confirmPwd')}/>
        {errors.confirmPwd && <p className="absolute mt-1 text-sm text-red-600">{errors.confirmPwd.message}</p>}
      </div>
      <button>Změnit heslo</button>
      {errors.apiError && <p className="mb-3 ml-3 text-sm text-red-600">{errors.apiError?.message}</p>}
      {isSuccessfullySubmitted && (<p className="mb-3 ml-3 text-sm text-green-600">Změna hesla proběhla v pořádku.</p>)}
      <Link to={`/login`}>Zpět na přihlášení</Link>
    </form>
  </section>
  )
}

export default NewPassword