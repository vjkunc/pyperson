import { useEffect } from 'react';
import { useRedirectLogoutUser } from '../custom-hooks/navigation';
import './ForgotPassword.scss';
import { Link, useNavigate } from "react-router-dom";
import axios from 'axios';
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import * as yup from 'yup'
import React, { useState } from 'react';

function ForgotPassword() {

    const navigate = useNavigate()
    const [ redirect ] = useRedirectLogoutUser()
    useEffect(()=>{
        if(redirect) navigate(redirect);
    },[]);
    function handleClick() {
        navigate("/");
    }
    type Form = {
        email: string;
        apiError: string;
      }
  
      const formSchema = yup.object().shape({
        email: yup.string()
          .required('Toto pole je povinné')
          .email('E-mail není ve validním formátu')
          ,
      })
  
      const {setError, register, handleSubmit, formState: { errors } } = useForm<Form>({ 
        resolver: yupResolver(formSchema)
      });
  
      const [isSuccessfullySubmitted, setIsSuccessfullySubmitted] = useState(false);
      const [checkbox, setCheckbox] = useState(false);
  
      const onSubmit = (data: Form) => {
        axios.post('/forgotPassword',{
          email: data.email
        })
        .then(res => {
          setIsSuccessfullySubmitted(true);
        }).catch(err => {
          console.error(err.response);
          if(err.response.data.errorMessage.includes("not found")) {
            setError('apiError', {
              type: "server",
              message: 'Uživatel se zadaným emailem nenalezen!',
            });
          }
          else {
            setError('apiError', {
              type: "server",
              message: 'Někde nastala chyba zkuste to znovu!',
            });
          }
        })
      }
  return (
    <section className="change-password">
    <h1>Žádost o obnovení hesla</h1>
    <form className="data" onSubmit={handleSubmit(onSubmit)}>
      <div className="w-full">
        <input placeholder="Zadejte e-mail" id="email" {...register("email")}/>
        {errors.email && <p className="absolute mt-1 text-sm text-red-600">{errors.email.message}</p>}
      </div>
      <button>Požádat o změnu hesla</button>
      {errors.apiError && <p className="mb-3 ml-3 text-sm text-red-600">{errors.apiError?.message}</p>}
      {isSuccessfullySubmitted && (<p className="mb-3 ml-3 text-sm text-green-600">Odkaz pro obnovení hesla byl zaslán na email.</p>)}
      <Link to={`/login`}>Zpět na přihlášení</Link>
    </form>
  </section>
  )
}

export default ForgotPassword