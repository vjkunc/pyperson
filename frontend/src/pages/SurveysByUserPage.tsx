import './SurveysByUserPage.scss';
import SurveyByUser from '../components/main/SurveysByUser';
import Header from '../components/main/Header';
import Footer from '../components/main/Footer';
import { useNavigate } from 'react-router-dom';
import { useEffect } from 'react';
import { useRedirectAdminUser } from '../custom-hooks/navigation';
import { useParams } from 'react-router-dom';

function SurveysByUserPage () {
    const navigate = useNavigate()
    const [ redirect ] = useRedirectAdminUser()
    useEffect(()=>{
        if(redirect) navigate(redirect);
    },[]);

    const { id } = useParams();
    return (
        <section>
            <Header></Header>
            <SurveyByUser userId={id}/>
            <Footer></Footer>
        </section>
    );
}

export default SurveysByUserPage