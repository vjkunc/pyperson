import { useNavigate } from "react-router-dom";
import Footer from '../components/main/Footer';

function Gdpr() {
    const navigate = useNavigate();
    function handleClick() {
        navigate("/");
    }
  return (
    <section className="flex flex-wrap items-center h-full">
    <div className="container flex flex-col items-center justify-center px-5 mx-auto my-8 text-gray-400" style={{paddingBottom: "6em"}}>
      <div className="max-w-md text-center">
        <div className="mx-auto max-w-screen-sm text-center lg:mb-16 mb-8">
          <h2 className="mb-4 text-3xl lg:text-4xl tracking-tight font-extrabold text-gray-900 ">GDPR</h2>
          <p className="font-light text-gray-500 sm:text-xl ">
          GDPR (General Data Protection Regulation) je nařízení Evropské unie, které upravuje ochranu osobních údajů občanů EU. Toto nařízení bylo přijato v květnu 2018 a nahradilo předchozí směrnici o ochraně osobních údajů.
          
          GDPR se vztahuje na všechny organizace, které zpracovávají osobní údaje občanů EU, bez ohledu na to, zda mají sídlo v EU nebo ne. GDPR stanovuje, že osobní údaje musí být zpracovávány zákonným a transparentním způsobem a že osoba, jejíž osobní údaje jsou zpracovávány, musí mít právo na informace o tom, jak jsou její údaje zpracovávány.

          GDPR stanovuje také, že organizace musí zajistit bezpečnost osobních údajů a mohou být vystaveny vysokým pokutám v případě porušení této povinnosti. Organizace musí také zajistit, že jsou splněny požadavky na přenositelnost a výmaz osobních údajů.

          GDPR také zavádí nové povinnosti pro organizace, jako je nutnost jmenování DPO (Data Protection Officer) v určitých případech a povinnost hlásit porušení ochrany osobních údajů do 72 hodin od jeho zjištění.

          GDPR je krokem k většímu respektování soukromí a ochrany osobních údajů občanů EU a má za cíl poskytnout uživatelům větší kontrolu nad tím, jak jsou jejich údaje zpracovávány a používány.
			    </p>
      	</div> 
			  <a rel="noopener noreferrer" href="#" className="px-8 py-3 font-semibold rounded text-white" onClick={handleClick} style={{background: "#2C6CB8"}}>Zpět na úvodní plochu</a>
      </div>
    </div>
    <Footer></Footer>
    </section>
  )
}

export default Gdpr