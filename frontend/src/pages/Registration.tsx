import { useEffect } from 'react';
import RegistrationWindow from '../components/registration/RegistrationWindow';
import './Registration.scss';
import { useNavigate } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux'
import type { RootState } from '../store/store'
import { useRedirectLogoutUser } from '../custom-hooks/navigation';

function Registration() {
  const navigate = useNavigate()
  const [ redirect ] = useRedirectLogoutUser()
  useEffect(()=>{
      if(redirect) navigate(redirect);
  },[]);
  return (
    <section className="registration">
      <RegistrationWindow></RegistrationWindow>
    </section>
  )
}

export default Registration