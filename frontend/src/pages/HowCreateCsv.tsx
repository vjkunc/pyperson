import { useNavigate } from "react-router-dom";
import Footer from '../components/main/Footer';
import { useEffect } from 'react';
import { useRedirectAdminUser } from '../custom-hooks/navigation';

function HowCreateCsv() {
  const navigate = useNavigate();
  const [ redirect ] = useRedirectAdminUser()
  useEffect(()=>{
    if(redirect) navigate(redirect);
  },[]);
  function handleClick() {
    navigate("/admin/new");
  }
  return (
    <section className="flex flex-wrap items-center h-full">
      <div className="container flex flex-col items-center justify-center px-5 mx-auto my-8 text-gray-400" style={{paddingBottom: "6em"}}>
        <div className="max-w-xl text-center">
          <div className="mx-auto max-w-screen-sm text-center lg:mb-16 mb-8">
            <h2 className="mb-4 text-3xl lg:text-4xl tracking-tight font-extrabold text-gray-900">Návod pro tvorbu CSV souboru</h2>
            <div className="font-light text-gray-500 sm:text-xl text-left">
              <ul style={{listStyle: "decimal"}}>
                <li style={{paddingBottom: "1em"}}>
                  <h3 style={{paddingBottom: "0.5em"}}><b>Vytvoření souboru</b></h3>
                  <p>Otevřete jakýkoli textový editor a uložte jej s příponou <b>.csv</b> (např. <i>dotaznik.csv</i>).</p>
                </li>
                <li style={{paddingBottom: "1em"}}>
                  <h3 style={{paddingBottom: "0.5em"}}><b>Struktura souboru</b></h3>
                  <p>Soubor bude dále rozdělen na dvě části - hlavičku a otázky.</p>
                  <p>Na první řádek souboru vložte následující text (hlavičku):<br/><b>question;description;required;typeOfAnswer</b></p>
                  <p>Od druhého řádku se vkládají konkrétní otázky, kde jeden řádek odpovídá jedné otázce.</p>
                </li>
                <li style={{paddingBottom: "1em"}}>
                  <h3 style={{paddingBottom: "0.5em"}}><b>Struktura otázky</b></h3>
                  <p>Jednotlivé informace otázky na řádku se od sebe oddělují středníky (;) bez přebytečných mezer.</p>
                  <p>Údaje otázky se zadávají následovně:</p>
                  <ul style={{listStyle: "decimal", marginLeft: "1.5em"}}>
                    <li>Text otázky</li>
                    <li>Popis otázky</li>
                    <li>Povinnost odpovědi ('<b>0</b>' = volitelná |'<b>1</b>' = povinná odpověď)</li>
                    <li>Typ odpovědi (hodnoty 1-5)</li>
                    <li>Možnosti odpovědí (podle typu odpovědi, oddělované středníky)</li>
                  </ul>
                  <p>Struktura řádku je tedy následující: <b>text otázky (text);další popis otázky (text/nic);povinnost otázky (0/1);typ otázky (0/1/2/3/4);dodatečné informace k odpovědím (nic/možnosti/min a max)</b> <br /></p>
                </li>
                <li style={{paddingBottom: "1em"}}>
                  <h3 style={{paddingBottom: "0.5em"}}><b>Typy odpovědí</b></h3>
                  <ul style={{listStyle: "inside"}}>
                    <li style={{paddingBottom: "0.5em"}}>
                      <b>Otevřená odpověď (1)</b>
                      <p style={{marginLeft: "1.5em"}}>
                        Odpověď libovolným textem. Tento typ odpovědi nemá žádné dodatečné informace. Příklad zapsání řádku:<br/>
                        <i>Zadejte název vaší oblíbené knihy;Prosím, pokuste se napsat přesný název;1;1</i>
                      </p>
                    </li>
                    <li style={{paddingBottom: "0.5em"}}>
                      <b>Výběr jedné z možností (2)</b>
                      <p style={{marginLeft: "1.5em"}}>
                        Možnost výběru jedné z několika možností (např. věková kategorie). Dodatečné informace pro tento typ odpovědi představují jednotlivé možnosti výběru (oddělené středníkem). Příklad zapsání řádku:<br/>
                        <i>Jaký je váš věk?;;1;2;<b>0-18;19-26;27-40;41-65;65+</b></i><br/>
                        Poznámka: Příklad uvání situaci, kdy uživatel nechce mít u otázky její popis (proto ';;')
                      </p>
                    </li>
                    <li style={{paddingBottom: "0.5em"}}>
                      <b>Výběr více možností najednou (3)</b>
                      <p style={{marginLeft: "1.5em"}}>
                        Možnost výběru více možností (např. preferované barvy). Dodatečné informace pro tento typ odpovědi představují jednotlivé možnosti výběru (oddělené středníkem). Příklad zapsání řádku:<br/>
                        <i>Vyberte všechny barvy, které máte rádi;;1;3;<b>červená;zelená;žlutá;modrá;fialová;oranžová;růžová;černá;bílá</b></i>
                      </p>
                    </li>
                    <li style={{paddingBottom: "0.5em"}}>
                      <b>Číselná odpověď (4)</b>
                      <p style={{marginLeft: "1.5em"}}>
                        Odpověď libovolnou hodnotou z intervalu (např. preferované číslo). Dodatečnou informaci pro tento typ odpovědi představuje minimum a maximum číselného rozsahu (oddělené středníkem). Příklad zapsání řádku:<br/>
                        <i>Zadejte číslo od 1 do 100, které se vám líbí;;1;4;<b>1;100</b></i>
                      </p>
                    </li>
                    <li style={{paddingBottom: "0.5em"}}>
                      <b>Datum (5)</b>
                      <p style={{marginLeft: "1.5em"}}>
                        Odpověď formou datumu. Tento typ odpovědi nemá žádné dodatečné informace. Příklad zapsání řádku:<br/>
                        <i>Zadejte datum vyplnění dotazníku;;1;5</i>
                      </p>
                    </li>
                  </ul>
                </li>
                <li style={{paddingBottom: "1em"}}>
                  <h3 style={{paddingBottom: "0.5em"}}><b>Příklad celého souboru</b></h3>
                  <p>
                    question;description;required;typeOfAnswer<br/>
                    Zadejte název vaší oblíbené knihy;Prosím, pokuste se napsat přesný název;1;1<br/>
                    Jaký je váš věk?;;1;2;0-18;19-26;27-40;41-65;65+<br/>
                    Vyberte všechny barvy, které máte rádi;;1;3;červená;zelená;žlutá;modrá;fialová;oranžová;růžová;černá;bílá<br/>
                    Zadejte číslo od 1 do 100, které se vám líbí;;1;4;1;100<br/>
                    Zadejte datum vyplnění dotazníku;;1;5
                  </p>
                </li>
                <li style={{paddingBottom: "1em"}}>
                  <h3 style={{paddingBottom: "0.5em"}}><b>Upozornění!</b></h3>
                  <p style={{paddingBottom: "0.5em"}}>Pokud budete používat jiný než textový editor (např. Excel), může dojít k nechtěné změně struktury souboru (např. přidání středníků navíc). Proto doporučujeme soubory po vytvoření zkontrolovat v textovém editoru.</p>
                  <p>Zkontrolujte, zda nějaký řádek nekončí středníkem. Nezapomeňte soubor nakonec uložit :)</p>
                </li>
              </ul>
            </div>
          </div> 
          <a rel="noopener noreferrer" href="#" className="px-8 py-3 font-semibold rounded text-white" onClick={handleClick} style={{background: "#2C6CB8"}}>Zpět ke tvorbě dotazníku</a>
        </div>
      </div>
      <Footer></Footer>
    </section>
  )
}

export default HowCreateCsv