import './SurveyPage.scss';
import SurveyDetail from '../components/survey/SurveyDetail';
import Header from '../components/main/Header';
import Footer from '../components/main/Footer';
import { useNavigate } from 'react-router-dom';
import { useEffect } from 'react';
import { useRedirectNormalUser } from '../custom-hooks/navigation';
import { Routes, Route, useParams } from 'react-router-dom';

function SurveyPage () {
    const navigate = useNavigate()
    const [ redirect ] = useRedirectNormalUser()
    useEffect(()=>{
        if(redirect) navigate(redirect);
    },[]);

    const { id } = useParams();
    return (
        <section>
            <Header></Header>
            <SurveyDetail formId={id} />
            <Footer></Footer>
        </section>
    );
}

export default SurveyPage