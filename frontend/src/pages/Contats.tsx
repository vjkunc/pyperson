import { useNavigate } from "react-router-dom";
import Footer from '../components/main/Footer';

function Contats() {
    const navigate = useNavigate();
    function handleClick() {
        navigate("/");
    }
  return (
    <section className="flex flex-wrap items-center h-full">
	<div className="container flex flex-col items-center justify-center px-5 mx-auto my-8 text-gray-400" style={{paddingBottom: "6em"}}>
		<div className="max-w-md text-center">
		<div className="mx-auto max-w-screen-sm text-center lg:mb-16 mb-8">
          <h2 className="mb-4 text-3xl lg:text-4xl tracking-tight font-extrabold text-gray-900 ">Kontakty</h2>
          <p className="font-light text-gray-500 sm:text-xl ">
			V případě problémů kontaktujte administrátora stránky na email Pavel.Beranek@ujep.cz
			</p>
      	</div> 
		<a rel="noopener noreferrer" href="#" className="px-8 py-3 font-semibold rounded text-white" onClick={handleClick} style={{background: "#2C6CB8"}}>Zpět na úvodní plochu</a>
		</div>
	</div>
	<Footer></Footer>
	</section>
  )
}

export default Contats