import './UsersPage.scss';
import Users from '../components/main/Users';
import Header from '../components/main/Header';
import Footer from '../components/main/Footer';

function UsersPage () {
    return (
        <section>
            <Header></Header>
            <Users></Users>
            <Footer></Footer>
        </section>
    );
}

export default UsersPage