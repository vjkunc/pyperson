import Header from '../components/main/Header';
import MainContent from '../components/main/MainContent';
import Footer from '../components/main/Footer';
import './MainPage.scss';
import { useNavigate } from 'react-router-dom';
import { useEffect } from 'react';
import { useRedirectNormalUser } from '../custom-hooks/navigation';

function MainPage() {
    const navigate = useNavigate()
    const [ redirect ] = useRedirectNormalUser()
    useEffect(()=>{
        if(redirect) navigate(redirect);
    },[]);

    return (
        <section className="main">
            <Header></Header>
            <MainContent></MainContent>
            <Footer></Footer>
        </section>
    );
}
  
export default MainPage