import LoginWindow from '../components/login/LoginWindow';
import Carousel from '../components/login/Carousel';
import './Login.scss';
import { useNavigate } from 'react-router-dom';
import { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux'
import type { RootState } from '../store/store'
import { useRedirectLogoutUser } from '../custom-hooks/navigation';

function Login() {
  const navigate = useNavigate()
  const [ redirect ] = useRedirectLogoutUser()
  useEffect(()=>{
      if(redirect) navigate(redirect);
  },[]);

  return (
    <section className="login">
      <Carousel></Carousel>
      <LoginWindow></LoginWindow>
    </section>
  )
}

export default Login