import { useEffect } from 'react';
import { useRedirectUser } from '../custom-hooks/navigation';
import './ChangePassword.scss';
import { Link, useNavigate } from "react-router-dom";
import axios from 'axios';
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import * as yup from 'yup'
import React, { useState } from 'react';
import { useSelector } from 'react-redux'
import type { RootState } from '../store/store'

function ChangePassword() {
    const navigate = useNavigate()
    const [ redirect ] = useRedirectUser()
    useEffect(()=>{
        if(redirect) navigate(redirect);
    },[]);

    // function handleClick() {
    //     navigate("/");
    // }

    type Form = {
        passwordNew: string;
        confirmPwd: string;
        password: string;
        apiError: string;
      }
  
      const formSchema = yup.object().shape({
        passwordNew: yup.string()
          .required('Toto pole je povinné')
          .min(6, 'Heslo musí být minimálně 6 znaků dlouhé')
          ,
        confirmPwd: yup.string()
          .required('Toto pole je povinné')
          .oneOf([yup.ref('passwordNew')], 'Hesla se neshodují')
          ,
        password: yup.string()
          .required('Toto pole je povinné')
          ,
      })
  
      const {setError, register, handleSubmit, formState: { errors } } = useForm<Form>({ 
        resolver: yupResolver(formSchema)
      });
  
      const [isSuccessfullySubmitted, setIsSuccessfullySubmitted] = useState(false);

      const userToken = useSelector((state: RootState) => state.user.token)
      const userEmail = useSelector((state: RootState) => state.user.email)
  
      const onSubmit = (data: Form) => {
        axios.post('/newPassword',
        {
          email: userEmail,
          newPassword: data.passwordNew,
          password: data.password
        },
        {
          headers: {
            Authorization: 'Bearer ' + userToken
          }
        })
        .then(res => {
          setIsSuccessfullySubmitted(true);
          setTimeout(() => {
            navigate('/');
          }, 6000);
        }).catch(err => {
          console.error(err.response);
          if(err.response.data.errorMessage.includes("Password")) {
            setError('password', {
              type: "server",
              message: 'Vaše heslo nebylo zadáno správně!',
            });
          }
          else {
            setError('apiError', {
              type: "server",
              message: 'Někde nastala chyba zkuste to znovu!',
            });
          }
        })
      }
  return (
    <section className="change-password">
    <h1>Změna hesla</h1>
    <form className="data" onSubmit={handleSubmit(onSubmit)}>
      <div className="w-full">
        <input type="password" placeholder="Nové heslo" id="passwordNew" {...register('passwordNew')}/>
        {errors.passwordNew && <p className="absolute mt-1 text-sm text-red-600">{errors.passwordNew.message}</p>}
      </div>
      <div className="w-full">
        <input type="password" placeholder="Ověření nového hesla" id="confirmPasswordNew" {...register('confirmPwd')}/>
        {errors.confirmPwd && <p className="absolute mt-1 text-sm text-red-600">{errors.confirmPwd.message}</p>}
      </div>
      <div className="w-full">
        <input type="password" placeholder="Aktuální heslo pro potvrzení" id="password" {...register('password')}/>
        {errors.password && <p className="absolute mt-1 text-sm text-red-600">{errors.password.message}</p>}
      </div>
      <button>Změnit heslo</button>
      {errors.apiError && <p className="mb-3 ml-3 text-sm text-red-600">{errors.apiError?.message}</p>}
      {isSuccessfullySubmitted && (<p className="mb-3 ml-3 text-sm text-green-600">Změna hesla proběhla v pořádku.</p>)}
      <Link to={`/`}>Zpět na úvodní stránku</Link>
    </form>
  </section>
  )
}

export default ChangePassword