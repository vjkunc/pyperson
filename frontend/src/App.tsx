import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom";
import Login from './pages/Login';
import Registration from './pages/Registration';
import MainPage from './pages/MainPage';
import MainPageAdmin from './pages/MainPageAdmin';
import UsersPage from './pages/UsersPage';
import SurveyByUserPage from './pages/SurveysByUserPage';
import NewSurvey from './pages/NewSurveyPage';
import SurveyPage from './pages/SurveyPage';
import Gdpr from './pages/Gdpr';
import AboutProject from './pages/AboutProject';
import NewPassword from './pages/NewPassword';
import ForgotPassword from "./pages/ForgotPassword";
import ChangePassword from "./pages/ChangePassword";
import Contats from './pages/Contats';
import HowCreateCsv from './pages/HowCreateCsv';
import { persistedStore, store } from './store/store';
import { PersistGate } from 'redux-persist/integration/react';
import { Provider } from 'react-redux';
import NotFound from "./pages/NotFound";


function App() {
    return (
        <Provider store={store}>
            <PersistGate loading={null} persistor={persistedStore}>
                <BrowserRouter>
                <Routes>
                    <Route path="/" element={<MainPage />} />
                    <Route path="admin" element={<MainPageAdmin />} />
                    <Route path="admin/new" element={<NewSurvey />} />
                    <Route path="admin/users" element={<UsersPage />} />
                    <Route path="admin/users/:id" element={<SurveyByUserPage />} />
                    <Route path="admin/howcreatecsv" element={<HowCreateCsv/>} />
                    <Route path="login" element={<Login/>} />
                    <Route path="registration" element={<Registration />} />
                    <Route path="survey/:id" element={<SurveyPage />} />
                    <Route path="gdpr" element={<Gdpr/>} />
                    <Route path="about" element={<AboutProject/>} />
                    <Route path="forgotpassword" element={<ForgotPassword/>} />
                    <Route path="newpassword/:token" element={<NewPassword/>} />
                    <Route path="changepassword" element={<ChangePassword/>} />
                    <Route path="contacts" element={<Contats/>} />
                    <Route path="404" element={<NotFound />} />
                    <Route path="*" element={<Navigate to ="/404" />}/>
                    {/* <Navigate to="/login"/> */}
                </Routes>
                </BrowserRouter>
            </PersistGate>
        </Provider>
    );
}

export default App