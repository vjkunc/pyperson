import './LoginWindow.scss';
import { Link, useNavigate } from "react-router-dom";
import React, { useState } from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { solid } from '@fortawesome/fontawesome-svg-core/import.macro';
import axios from 'axios';
import * as yup from 'yup'
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import { useSelector, useDispatch } from 'react-redux'
import { changeID, changeEmail, changeToken, changeRole } from '../../store/userSlice'
import type { RootState } from '../../store/store'

function LoginWindow(props: any) {
    const dispatch = useDispatch()
    // Initialize a boolean state
    const [passwordShown, setPasswordShown] = useState(false);

    type Form = {
      email: string;
      password: string;
      apiError: string
    }


    const formSchema = yup.object().shape({
      email: yup.string()
        .required('Toto pole je povinné')
        .email('E-mail není ve validním formátu')
        ,
      password: yup.string()
        .required('Toto pole je povinné')
        ,
    })

    const {setError, register, handleSubmit, formState: { errors } } = useForm<Form>({ 
      resolver: yupResolver(formSchema)
    });


    const navigate = useNavigate()

    // Password toggle handler
    const togglePassword = (e: { stopPropagation: () => void; } ) => {
      e.stopPropagation();
      setPasswordShown(!passwordShown);
    };
    const glayClass = passwordShown ? '' : ' text-slate-500';
    const eyeClasses = `absolute select-none top-10 right-5 cursor-pointer ${glayClass}`;

    // TODO: Implementovat access token (bearer token pro autorizaci)
    const onSubmit = (data: Form) => {
          axios.post('login',{
            email: data.email,
            password: data.password
          })
          .then(res => {
            dispatch(changeToken(res.data.accessToken));
            dispatch(changeID(res.data.data._id));
            dispatch(changeEmail(res.data.data.email));
            dispatch(changeRole(res.data.data.role));
            navigate('/');
          }).catch(err => {
            if (err.response) {
              console.error(err.response);
            }
            if(err.response.statusText == 'UNAUTHORIZED'){
              setError('apiError', {
                type: "server",
                message: 'Neplatné heslo!',
              });
            } else if(err.response.statusText == 'NOT FOUND'){
                setError('apiError', {
                  type: "server",
                  message: 'Neplatný email!',
                });
            } else {
              setError('apiError', {
                type: "server",
                message: 'Někde nastala chyba!',
              });
            }
            
          })
        }

    return (
      <section className="login_window">
        <h1>Pyperson</h1>
        <form className="data" onSubmit={handleSubmit(onSubmit)}>
          <div className="w-full">
            <input placeholder="Zadejte e-mail" {...register("email")}></input>
            {errors.email && <p className="absolute mt-1 text-sm text-red-600">{errors.email.message}</p>}
          </div>
          <div className="w-full">
            <div className="flex flex-col relative">
              <input type={passwordShown ? "text" : "password"} placeholder="Zadejte heslo" {...register("password")}></input>
              <FontAwesomeIcon className={eyeClasses}
                onClick={togglePassword} 
                icon={passwordShown ? solid('eye') : solid('eye-slash')} 
                />
            </div>
            {errors.password && <p className="absolute mt-1 text-sm text-red-600">{errors.password.message}</p>}
          </div>
          <button className="select-none">Přihlásit se</button>
          {errors.apiError && <p className="mb-3 ml-3 text-sm text-red-600">{errors.apiError?.message}</p>}
          <Link to={`/forgotpassword`}>Obnovit heslo</Link>
          <Link to={`/registration`}>Nemáte účet? Zaregistrujte se</Link>
        </form>
        <footer className="refs">
            <Link to={`/about`}>O projektu</Link>
            <Link to={`/gdpr`}>GDPR</Link>
            <Link to={`/contacts`}>Kontakty</Link>
        </footer>
    </section>
    )
  }

  export default LoginWindow