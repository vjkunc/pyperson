import './Carousel.scss';
// Import Swiper React components
import { Swiper, SwiperSlide } from 'swiper/react';
import axios from 'axios';
import { useEffect, useState } from 'react';
// Import Swiper styles
import 'swiper/css';
import "swiper/css/pagination";
import "swiper/css/navigation";
// import required modules
import { Autoplay, Pagination, Navigation } from "swiper";
import { number } from 'yup';

function Carousel() {

    const [newForms, setNewForms] = useState<any>(null)
    useEffect(() => { 
        axios.get('/lastThreeForms')
        .then(response => {
            const res = response.data
            setNewForms(res.forms)
        }).catch(err => {
            if (err.response) {
                console.error(err.response);
            }
        })
    }, []);
    return (
        <article className="carousel">
            <Swiper 
                slidesPerView={1}
                centeredSlides={true}
                loop={true}
                autoplay={{
                delay: 5000,
                disableOnInteraction: true
                }}
                navigation={true}
                modules={[Autoplay, Pagination, Navigation]}
                >
                {
                    newForms ?
                    newForms.map((form:any, index: number) => 
                        <SwiperSlide key={index}>
                            <div className='content'>
                                <div className='text-3xl'>
                                    {form.title}
                                </div>
                                <div className='text'>
                                    {form.description}
                                </div>
                            </div>
                        </SwiperSlide>
                    )
                    : <SwiperSlide>
                    <div className='content'>
                        <div className='text-3xl'>
                            První dotazník
                        </div>
                        <div className='text'>
                            Přidej svůj první dotazník
                        </div>
                    </div>
                </SwiperSlide>
                }
                
            </Swiper>
        </article>
    );
}

  export default Carousel