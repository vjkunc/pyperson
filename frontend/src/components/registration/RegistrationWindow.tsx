import './RegistrationWindow.scss';
import { Link, useNavigate } from "react-router-dom";
import axios from 'axios';
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import * as yup from 'yup'
import React, { useState } from 'react';
import {ROLES} from '../../GlobalConstants'

//created by:
//https://www.positronx.io/add-confirm-password-validation-in-react-with-hook-form/
//https://medium.com/geekculture/how-to-add-react-hook-form-with-typescript-to-react-js-application-beginner-guide-4115b5525b0f
function RegistrationWindow() {
    type Form = {
      email: string;
      nickname: string;
      password: string;
      confirmPwd: string;
      apiError: string;
      confirmGDPR: boolean;
    }

    const formSchema = yup.object().shape({
      email: yup.string()
        .required('Toto pole je povinné')
        .email('E-mail není ve validním formátu')
        ,
      nickname: yup.string()
        .required('Toto pole je povinné')
        .min(4, 'Nickname musí být minimálně 4 znaky dlouhé')
        ,
      password: yup.string()
        .required('Toto pole je povinné')
        .min(6, 'Heslo musí být minimálně 6 znaků dlouhé')
        ,
      confirmPwd: yup.string()
        .required('Toto pole je povinné')
        .oneOf([yup.ref('password')], 'Hesla se neshodují')
        ,
      confirmGDPR: yup.boolean()
        .oneOf([true], "Pro registraci musíte potvrdit GDPR.")
        ,
    })

    const {setError, register, handleSubmit, formState: { errors } } = useForm<Form>({ 
      resolver: yupResolver(formSchema)
    });

    const navigate = useNavigate()

    const [isSuccessfullySubmitted, setIsSuccessfullySubmitted] = useState(false);
    const [checkbox, setCheckbox] = useState(false);

    const onSubmit = (data: Form) => {
      axios.post('signup',{
        email: data.email,
        nickname: data.nickname,
        password: data.password,
        role: ROLES.BASIC_USER
      })
      .then(res => {
        setIsSuccessfullySubmitted(true);
        setTimeout(() => {
          navigate('/login');
        }, 3000);
      }).catch(err => {
        console.error(err.response);
        if(err.response.data.errorMessage.includes("email")) {
          setError('email', {
            type: "server",
            message: 'Tento email je již používán',
          });
        }
        else if (err.response.data.errorMessage.includes("nickname")) {
          setError('nickname', {
            type: "server",
            message: 'Tento nickname je již používán',
          });
        }
        else {
          setError('apiError', {
            type: "server",
            message: 'Někde nastala chyba zkuste to znovu!',
          });
        }
      })
    }

    return (
      <section className="registration_window">
        <h1>Vytvořit účet</h1>
        <form className="data" onSubmit={handleSubmit(onSubmit)}>
          <div className="w-full">
            <input placeholder="Zadejte e-mail" id="email" {...register("email")}/>
            {errors.email && <p className="absolute mt-1 text-sm text-red-600">{errors.email.message}</p>}
          </div>
          <div className="w-full">
            <input placeholder="Zadejte nickname" id="nickname" {...register("nickname")}/>
            {errors.nickname && <p className="absolute mt-1 text-sm text-red-600">{errors.nickname.message}</p>}
          </div>
            <div className="w-full">
            <input type="password" placeholder="Zadejte heslo" id="password" {...register('password')}/>
            {errors.password && <p className="absolute mt-1 text-sm text-red-600">{errors.password.message}</p>}
          </div>
          <div className="w-full">
            <input type="password" placeholder="Ověření hesla" id="confirmPassword" {...register('confirmPwd')}/>
            {errors.confirmPwd && <p className="absolute mt-1 text-sm text-red-600">{errors.confirmPwd.message}</p>}
          </div>
          <div className="w-full">
            <div className='checkbox_field'>
              <input type="checkbox" id="confirmGDPR" {...register('confirmGDPR')}/>
              <div>Potvrzuji zpracování osobních údajů.</div> <input className='cursor-pointer' onClick={() => navigate("/gdpr")} type="button" value="více info" />
            </div>
            {errors.confirmGDPR && <p className="absolute mt-1 text-sm text-red-600">{errors.confirmGDPR.message}</p>}
          </div>
          <button>Registrovat se</button>
          {errors.apiError && <p className="mb-3 ml-3 text-sm text-red-600">{errors.apiError?.message}</p>}
          {isSuccessfullySubmitted && (<p className="mb-3 ml-3 text-sm text-green-600">Registrace proběhla v pořádku.</p>)}
          <Link to={`/login`}>Zpět na přihlášení</Link>
        </form>
      </section>
    )
  }

  export default RegistrationWindow