import './SurveyDetail.scss';
import { useEffect, useState } from 'react';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux'
import { changeToken } from '../../store/userSlice'
import type { RootState } from '../../store/store'

function SurveyDetail (props: any) {
    const [showedAnswer, setShowedAnswer] = useState<number>(-1)
    const [form, setForm] = useState<any>()
    const [formAnswers, setFormAnswers] = useState<any>()
    const userID = useSelector((state: RootState) => state.user.id)
    const userToken = useSelector((state: RootState) => state.user.token)
    const dispatch = useDispatch()
    const navigate = useNavigate()
    useEffect(() => { 
        axios.get('/form/' + props.formId
        , { 
            headers: {
                Authorization: 'Bearer ' + userToken
                }
            }
        )
        .then(response => {
            const res = response.data
            res.access_token && dispatch(changeToken(res.access_token));
            setForm(res.form)
            setFormAnswers(new Array(res.form.questions.length))
        }).catch(err => {
            if (err.response) {
                console.error(err.response);
                if(err.response.status === 401){
                    dispatch(changeToken(""))
                    navigate('/login');
                }
            }
        })
    }, []);

    const handleInput = (inputEv:any, index:any) => {
        const value = inputEv.target.value;
        const answers = JSON.parse(JSON.stringify(formAnswers));
        answers[index] = value
        setFormAnswers([...answers]);
        console.log(answers)
      };

    const handleSubmit = () => {
        const data = {
            answers: formAnswers,
            userAnsweredId: userID,
            formId: props.formId,
        };
        axios.post('/createAnswer', data
        , { 
            headers: {
                Authorization: 'Bearer ' + userToken
                }
            }
        )
        .then(response => {
            navigate('/');
        }).catch(err => {
            if (err.response) {
                console.error(err.response);
                if(err.response.status === 401){
                    dispatch(changeToken(""))
                    navigate('/login');
                }
            }
        })
      }

    const handleCheckInput = (inputEv:any, i:any ,index:any, length:any) => {
        let value = null
        if(inputEv.target.checked){
            value = inputEv.target.value;
        } 
        const answers = JSON.parse(JSON.stringify(formAnswers));
        if(answers[index] == null)
            answers[index] = new Array(length, null)
        answers[index][i] = value
        setFormAnswers([...answers]);
        console.log(answers)
    }

    const isURL = (url:any) => {
        try {
            new URL(url);
            return true;
        } catch (_) {
            return false;
        }
    }
    
    return (
        <main className="survey_detail">
            {form ? 
            <div className='form'>
                {-1 == showedAnswer ?
                (<div className="question">
                    <div className="survey-header">
                        <h1>{form.title}</h1>
                        <p>{form.description}</p>
                    </div>
                    <button type="button" onClick={() => setShowedAnswer(0)} name="action">Začít</button>
                </div>)
                : null}
                {form.questions.map((item:any, index:any) => {
                    return (
                        <form className="question" key={index}  
                            onSubmit={(event:any) => {
                                event.preventDefault()
                                if(item.required === true){
                                    if(formAnswers[index] && formAnswers[index] !== '' && (Array.isArray(formAnswers[index]) ? formAnswers[index].some((el: null) => el !== null) : true) ) {
                                        setShowedAnswer(showedAnswer + 1)
                                    }
                                } else {
                                    setShowedAnswer(showedAnswer + 1)
                        }}}>
                            {(() => {
                                switch (item.typeOfAnswer) {
                                    case "open":
                                        return  (
                                            <fieldset style={{display: index == showedAnswer ? 'flex' : 'none' }}>
                                                <label >{item.question}</label>
                                                {isURL(item.description) ? <img src={item.description} /> : <p>{item.description}</p>}
                                                <input onChange={(e) => handleInput(e, index)} type="text" name={item.question} id={item.question}/>
                                            </fieldset>
                                        ) 
                                        break;
                                    case "select":
                                        return (
                                            <fieldset style={{display: index == showedAnswer ? 'flex' : 'none' }}>
                                                <label  >{item.question}</label>
                                                {isURL(item.description) ? <img src={item.description} /> : <p>{item.description}</p>}
                                                {item.options.map((option:any, i:any) => {
                                                    return (
                                                        <div key={i}>
                                                            <input onChange={(e) => handleInput(e, index)} value={option} type="radio" name={item.question} id={item.question}></input>
                                                            {isURL(option) ? <img src={option} style={{ width: "300px", height: "auto" }}/> : option}
                                                        </div>
                                                    );
                                                })}
                                            </fieldset>
                                        )
                                        break;
                                    case "check":
                                        return (
                                            <fieldset style={{display: index == showedAnswer ? 'flex' : 'none' }}>
                                                <label >{item.question}</label>
                                                {isURL(item.description) ? <img src={item.description} /> : <p>{item.description}</p>}
                                                {item.options.map((option:any, i:any) => {
                                                    return (
                                                        <div key={i}>
                                                            <input type="checkbox" onChange={(e) => handleCheckInput(e, i, index, item.options.length)} value={option} name={item.question} id={item.question}></input>
                                                            {isURL(option) ? <img src={option} style={{ width: "300px", height: "auto" }}/> : option}
                                                        </div>
                                                    );
                                                })}
                                            </fieldset>
                                        )
                                        break;
                                    case "number":
                                        return (
                                            <fieldset style={{display: index == showedAnswer ? 'flex' : 'none' }}>
                                                <label >{item.question}</label>
                                                {isURL(item.description) ? <img src={item.description} /> : <p>{item.description}</p>}
                                                <input type="number" onChange={(e) => handleInput(e, index)} name={item.question} id={item.question} min={item.options[0].trim()} max={item.options[1].trim()}/>
                                            </fieldset>
                                        )
                                        break;
                                    case "date":
                                        return (
                                            <fieldset style={{display: index == showedAnswer ? 'flex' : 'none' }}>
                                                <label >{item.question}</label>
                                                {isURL(item.description) ? <img src={item.description} /> : <p>{item.description}</p>}
                                                <input type="date" onChange={(e) => handleInput(e, index)} name={item.question} id={item.question}/>
                                            </fieldset>
                                        )
                                        break;
                                    default:
                                        console.log("Here should be error handling!")
                                }
                            })()}
                            {index == showedAnswer && item.required ?
                            (<div className='flex justify-start ml-2'>*Odpověď je povinná</div>)
                            :null
                            }
                            {index == showedAnswer ?
                            (<div className='before-next'>
                                <button 
                                    onClick={() => setShowedAnswer(showedAnswer - 1)} 
                                    >
                                    Předchozí
                                </button>
                                {form.questions.length !== showedAnswer ? (
                                <button type='submit'
                                        onClick={() => {
                                            
                                        }} 
                                    >
                                    Další
                                </button>) : null}
                            </div>) : ""
                            }
                        </form>
                    );
                })}
                {
                form.questions.length == showedAnswer ?
                    (
                        <div className='question'>
                            <div className="survey-header">
                                <h1>Odeslat odpovědi</h1>
                                <p>Odpovědi je možné ještě upravit, pro odeslání klikněte na tlačítko odeslat.</p>
                            </div>
                            <div className='before-next'>
                                <button
                                    onClick={() => setShowedAnswer(showedAnswer - 1)} 
                                    >
                                    Předchozí
                                </button>
                                <button name="action" onClick={handleSubmit}>Odeslat</button>
                            </div>

                        </div>
                    ) : null
                }
            </div>
            : "Chyba při načítání!" }
        </main>
    );
}

export default SurveyDetail