import './Survey.scss';
import CsvInput from './CsvInput';
import { useState } from 'react';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { changeToken } from '../../store/userSlice';
import type { RootState } from '../../store/store';

function Survey () {
    const [questions, setQuestins] = useState([{}]);
    const [name, setName] = useState("");
    const [desc, setDesc] = useState("");
    const [time, setTime] = useState("01:00");
    const [isInvalid, setIsInvalid] = useState(false);
    const [favorite, setFavorite] = useState(false);
    const navigate = useNavigate()
    const userToken = useSelector((state: RootState) => state.user.token)
    const userID = useSelector((state: RootState) => state.user.id)
    const dispatch = useDispatch()

    const submit = (event:any) => {
        event.preventDefault();
        
        if(isInvalid) return

        const timeParts = time.split(":");
        const lengthInMinutes = Number(timeParts[0]) * 60 + Number(timeParts[1]);
        let data = {
            author: userID,
            lengthInMinutes: lengthInMinutes,
            description: desc,
            title: name,
            questions: questions,
            recommended: favorite
        }
        axios.post('/createForm', data
          , { 
            headers: {
                Authorization: 'Bearer ' + userToken
                }
            }
          )
          .then(response => {
            const res = response.data
            res.access_token && dispatch(changeToken(res.access_token));
            navigate('/');
          }).catch(err => {
            if (err.response) {
              console.error(err.response);
              if(err.response.status === 401){
                dispatch(changeToken(""))
                navigate('/login');
            }
            }
          })
    }
    return (
        <main className="survey">
            <form action="" onSubmit={submit}>
                <h1>Nový dotazník</h1>
                <table>
                    <tr>
                        <td><label htmlFor="survey_name">Název dotazníku:</label></td>
                        <td><input type="text" name="survey_name" id="name" onChange={(e:any) => setName(e.target.value)} required/></td>
                    </tr>
                    <tr>
                        <td><label htmlFor="survey_desc">Popis:</label></td>
                        <td><input type="text" name="survey_desc" id="desc" onChange={(e:any) => setDesc(e.target.value)} required/></td>
                    </tr>
                    <tr>
                        <td><label htmlFor="survey_time">Odhadovaný čas:</label></td>
                        <td><input type="time" name="survey_time" id="time" min="00:00" value={time} max="23:59" onChange={(e:any) => setTime(e.target.value)} required/></td>
                    </tr>
                    <tr>
                        <td><label htmlFor="favorite">Doporučený dotazník:</label></td>
                        <td><input type="checkbox" name="favorite" id="favorite" checked={favorite} onChange={(e:any) => setFavorite(e.target.checked)}/></td>
                    </tr>
                    <CsvInput setQuestins={setQuestins} setIsInvalid={setIsInvalid} isInvalid={isInvalid}></CsvInput>
                </table>
                <p>Návod na vytvoření CSV souboru naleznete<a href="/admin/howcreatecsv" className="csvTutorial">zde</a></p>
                {isInvalid ? (<div className='error'>CSV soubor nesplňuje požadavky!</div>) : null}
                <input type="submit" name="action" value="Vytvořit"></input>
            </form>
        </main>
    );
}

export default Survey