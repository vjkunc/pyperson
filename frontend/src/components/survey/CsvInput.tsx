import './CsvInput.scss';
import Papa from "papaparse";

import * as yup from 'yup';

let questionsSchema = yup.object().shape({
    values: yup.array().of(
        yup.object({
            question: yup.string().required(),
            description: yup.string(),
            required: yup.boolean().required(),
            typeOfAnswer: yup.string().required(),
            options: yup.array().nullable(),
        })
    )
  });

function CsvInput (props: any) {
    const changeHandler = (event: any) => {
        // Passing file data (event.target.files[0]) to parse using Papa.parse
        Papa.parse(event.target.files[0], {
          header: true,
          skipEmptyLines: true,
          complete: function (results: any) {
            let invalid = false;
            results.data.forEach((item: any) => {
                item.options = item.__parsed_extra ? item.__parsed_extra : null;
                delete item.__parsed_extra;
                item.required = item.required == 1 ? true : false
                switch (item.typeOfAnswer.trim()) {
                    case "1":
                        item.typeOfAnswer = "open";
                        break;
                    case "2":
                        item.typeOfAnswer = "select";
                        break;
                    case "3":
                        item.typeOfAnswer = "check";
                        break;
                    case "4":
                        item.typeOfAnswer = "number";
                        break;
                    case "5":
                        item.typeOfAnswer = "date";
                        break;
                    default:
                        invalid = true
                }
              });
            console.log(results.data)
            props.setQuestins(results.data)
            questionsSchema.isValid({values: results.data}).then((valid) => {
                if(!valid || invalid){
                    props.setIsInvalid(true)
                } else {
                    props.setIsInvalid(false);
                }
            });
          },
        });
      };
    return (
        <tr>
            <td><label htmlFor="csv">Otázky:</label></td>
            <td><input 
                type="file"
                name="file"
                accept=".csv"
                onChange={changeHandler}
                required/>
            </td>
        </tr>
    );
}

export default CsvInput