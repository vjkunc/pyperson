import './Users.scss';
import User from './User';
import Navbar from './mainContent/Navbar';
import axios from 'axios';
import { useSelector, useDispatch } from 'react-redux';
import { changeToken } from '../../store/userSlice';
import type { RootState } from '../../store/store';
import { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

function Users () {
    const navigate = useNavigate();
    const authorId = useSelector((state: RootState) => state.user.id);
    const userToken = useSelector((state: RootState) => state.user.token);
    const [users, setUsers] = useState([]);
    const dispatch = useDispatch();
    const userIds: string[] = [];

    useEffect(() => {
        const func = async () => {
            await axios.get('/forms/' + authorId, {
                headers: {
                    Authorization: 'Bearer ' + userToken
                }
            }).then((response: any) => {
                const res = response.data;
                res.access_token && dispatch(changeToken(res.access_token));
                let fOverviews = res.forms;
                //Získání ID uživatelů bez duplicit
                fOverviews.map((form: any) => {
                    for (const user of form.usersAnswered) {
                        if (!userIds.includes(user)) userIds.push(user);
                    }
                });
            }).catch((err: any) => {
                if (err.response) {
                    console.error(err.response);
                    if(err.response.status === 401){
                        dispatch(changeToken(""))
                        navigate('/');
                    }
                }
            })
            await axios.post('/users', {userIds: userIds}, {
                headers: {
                    Authorization: 'Bearer ' + userToken
                }
            }).then((response: any) => {
                const res = response.data;
                res.access_token && dispatch(changeToken(res.access_token));
                setUsers(res.users);
            }).catch((err: any) => {
                if (err.response) {
                    console.error(err.response);
                    if(err.response.status === 401){
                        dispatch(changeToken(""))
                        navigate('/');
                    }
                }
            })
        }
        func()
    }, []);

    return (
        <section className="users">
            <Navbar/>
            <section className="list">
                {users.map((user: any, i: any) => {
                    return <User {...user} key={i}/>
                })}
            </section>
        </section>
    );
}

export default Users