import { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import './FormOverview.scss';

function FormOverview (props: any) {
    const navigate = useNavigate()
    const date = props.tsCreated.substring(0,10)
    return (
        <article className={"form_overview "+ (props.usersAnswered.includes(props.userID) ? 'done' : '')} 
                onClick={() => props.usersAnswered.includes(props.userID) || navigate('/survey/'+props._id) }>
            <section className="info">
                <h2>{props.title}</h2>
                <p>Vytvořeno: {date}</p>
            </section>
            {/*<section className="questions">
                {props.questionsPreview.map((item: string) => <p>{item}</p>)}
            </section>*/}
            <section className="stats">
                <p>Odhadovaná doba vyplnění: {props.lengthInMinutes}min</p>
                <p>Počet vyplnění: {props.completed}</p>
                <p>Počet otázek: {props.questions}</p>
            </section>
        </article>
    );
}

export default FormOverview;