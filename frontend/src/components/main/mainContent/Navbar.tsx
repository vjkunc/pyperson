function Navbar() {
    return (
        <nav>
            <a href="/admin">Dotazníky</a>
            <a href="/admin/users">Uživatelé</a>
            <a href="/admin/new">Přidat dotazník</a>
        </nav>
    );
}

export default Navbar