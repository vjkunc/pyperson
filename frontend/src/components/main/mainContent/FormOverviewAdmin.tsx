import { useState } from 'react';
import { boolean } from 'yargs';
import './FormOverviewAdmin.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { solid } from '@fortawesome/fontawesome-svg-core/import.macro';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import type { RootState } from '../../../store/store';
import { changeToken } from '../../../store/userSlice';

function FormOverview (props: any) {
    const date = props.tsCreated.substring(0,10)
    const downloadTxtFile = () => {
        const element = document.createElement("a");
        const file = new Blob([JSON.stringify(props.questions)], {type: 'text/plain'});
        element.href = URL.createObjectURL(file);
        element.download = "questions.txt";
        document.body.appendChild(element); // Required for this to work in FireFox
        element.click();
    }

    const exportAnswers = () => {
        axios.get('/answers/' + props._id
        , { 
            headers: {
                Authorization: 'Bearer ' + userToken
                }
            }
        )
        .then(response => {
            const element = document.createElement("a");
            const file = new Blob([JSON.stringify(response.data.data)], {type: 'text/plain'});
            element.href = URL.createObjectURL(file);
            element.download = "answers.txt";
            document.body.appendChild(element); // Required for this to work in FireFox
            element.click();
        }).catch(err => {
            if (err.response) {
                console.error(err.response);
                if(err.response.status === 401){
                    dispatch(changeToken(""))
                    navigate('/login');
                }
            }
        })
    }

    const [checked, setChecked] = useState(props.recommended)
    const [removed, setRemoved] = useState(false)
    const navigate = useNavigate()
    const dispatch = useDispatch()
    const userToken = useSelector((state: RootState) => state.user.token)

    const changeRecommended = () => {
        axios.post('/setRecommended/' + props._id, {}
        , { 
            headers: {
                Authorization: 'Bearer ' + userToken
                }
            }
        )
        .then(response => {
            setChecked(!checked) 
        }).catch(err => {
            if (err.response) {
                console.error(err.response);
                if(err.response.status === 401){
                    dispatch(changeToken(""))
                    navigate('/login');
                }
            }
        })
    }

    const deleteForm = (e: any) => {
        axios.post('/deleteForm/' + props._id, {}
        , { 
            headers: {
                Authorization: 'Bearer ' + userToken
                }
            }
        )
        .then(response => {
            setRemoved(true) 
        }).catch(err => {
            if (err.response) {
                console.error(err.response);
                if(err.response.status === 401){
                    dispatch(changeToken(""))
                    navigate('/login');
                }
            }
        })
    }

    return !removed ? (
        <article className="form_overview">
            <marker className="star">
                <div className="wrapper">
                    <input type="checkbox" checked={checked} onChange={changeRecommended}/>
                    {/* <label htmlFor={props._id}></label> */}
                    <span className="tooltiptext">Doporučený dotazník</span>
                </div>
            </marker>
            <section className="info">
                <h2>{props.title}</h2>
                <p>Vytvořeno: {date}</p>
            </section>
            <section className="stats">
                <p>Odhadovaná doba vyplnění: {props.lengthInMinutes}min</p>
                <p>Počet vyplnění: {props.completed}</p>
                <p>Počet otázek: {props.questionsCount}</p>
            </section>
            <section className="options">
                <FontAwesomeIcon icon={solid("file-arrow-down")} onClick={exportAnswers} title="Exportovat odpovědi" className="export_answers"/>
                <FontAwesomeIcon icon={solid("clipboard-question")} onClick={downloadTxtFile} title="Exportovat otázky" className="export_questions"/>
                <FontAwesomeIcon icon={solid("trash-can")} onClick={deleteForm} title="Odstranit dotazník" className="delete"/>
            </section>
        </article>
    ) : null;
}

export default FormOverview;