import './User.scss';
import { useNavigate } from 'react-router-dom';

function User (props: any) {
    const navigate = useNavigate();
    const date = props.tsCreated.substring(0, 10);
    return (
        <article className="user" onClick={() => navigate('/admin/users/' + props._id)}>
            <section className="info">
                <h2>{props.nickname}</h2>
                <p>{date}</p>
            </section>
        </article>
    );
}

export default User;