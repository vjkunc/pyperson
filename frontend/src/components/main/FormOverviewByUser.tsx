import { boolean } from 'yargs';
import './FormOverviewByUser.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { solid } from '@fortawesome/fontawesome-svg-core/import.macro';
import axios from 'axios';
import { useNavigate, useParams } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux'
import type { RootState } from '../../store/store'
import { changeToken } from '../../store/userSlice'

function FormOverviewByUser (props: any) {
    const date = props.tsCreated.substring(0,10);
    const navigate = useNavigate()
    const dispatch = useDispatch()
    const userToken = useSelector((state: RootState) => state.user.token)
    const { id } = useParams();

    const exportAnswers = () => {
        axios.get('/answer/' + id + '/' + props._id
        , { 
            headers: {
                Authorization: 'Bearer ' + userToken
                }
            }
        )
        .then(response => {
            const element = document.createElement("a");
            const file = new Blob([JSON.stringify(response.data.data.answers)], {type: 'text/plain'});
            element.href = URL.createObjectURL(file);
            element.download = "answers.txt";
            document.body.appendChild(element); // Required for this to work in FireFox
            element.click();
        }).catch(err => {
            if (err.response) {
                console.error(err.response);
                if(err.response.status === 401){
                    dispatch(changeToken(""))
                    navigate('/login');
                }
            }
        })
    }

    return (
        <article className="formOverviewByUser">
            <section className="info">
                <article>
                    <h2>{props.title}</h2>
                    <p>Popis: {props.description}</p>
                    <p>Vytvořeno: {date}</p>
                </article>
            </section>
            <section className="options">
                <FontAwesomeIcon icon={solid("file-arrow-down")} onClick={exportAnswers} title="Exportovat odpovědi" className="export_answers"/>
            </section>
        </article>
    );
}

export default FormOverviewByUser;