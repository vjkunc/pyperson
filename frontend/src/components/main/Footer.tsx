import './Footer.scss';

function Footer () {
    return (
        <footer className='footer'>
            <section>
                <a href="/about">O projektu</a>
                <a href="/gdpr">GDPR</a>
                <a href="/contacts">Kontakty</a>
            </section>
        </footer>
    );
}

export default Footer