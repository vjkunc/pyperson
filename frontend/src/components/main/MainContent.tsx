import './MainContent.scss';
import FormOverview from './mainContent/FormOverview';
import axios from 'axios';
import { useSelector, useDispatch } from 'react-redux'
import { changeToken } from '../../store/userSlice'
import type { RootState } from '../../store/store'
import { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

function MainContent () {
    const navigate = useNavigate()
    const [formOverviews, setformOverviews] = useState<any[]>([])
    const [selected, setSelected] = useState("recommended")
    const userID = useSelector((state: RootState) => state.user.id)
    const userToken = useSelector((state: RootState) => state.user.token)
    const dispatch = useDispatch()
    useEffect(() => {
        axios.get('/allForms'
        , {
            headers: {
                Authorization: 'Bearer ' + userToken
                }
            }
        )
        .then(response => {
            const res = response.data
            res.access_token && dispatch(changeToken(res.access_token));
            let fOverviews = res.forms
            for (let formOverview of fOverviews){
                formOverview.completed = formOverview.usersAnswered.length
                formOverview.questions = formOverview.questions.length
            }
            setformOverviews(fOverviews)
        }).catch(err => {
            if (err.response) {
                console.error(err.response);
                if(err.response.status === 401){
                    dispatch(changeToken(""))
                    navigate('/login');
                }
            }
        })
    }, []);

    return (
        <main className='mainContent'>
            <nav>
                <button className={selected == "recommended" ? "selected" : ""} onClick={() => setSelected("recommended")}>Doporučeno</button>
                <button className={selected == "notfilled" ? "selected" : ""} onClick={() => setSelected("notfilled")}>Nevyplněné</button>
                <button className={selected == "filled" ? "selected" : ""} onClick={() => setSelected("filled")}>Vyplněné</button>
            </nav>
            <section>
            {formOverviews && selected == "recommended" ? 
                formOverviews.map((formOverview: any, i: any) => {
                    if(formOverview.recommended == true && !formOverview.usersAnswered.includes(userID))
                        return <FormOverview {...formOverview} key={i} userID={userID}/>
                } 
            ) : ""}
            {formOverviews && selected == "notfilled" ? 
                formOverviews.map((formOverview: any, i: any) => {
                    if(!formOverview.usersAnswered.includes(userID))
                        return <FormOverview {...formOverview} key={i} userID={userID}/>
                } 
            ) : ""}
            {formOverviews && selected == "filled" ? 
                formOverviews.map((formOverview: any, i: any) => {
                    if(formOverview.usersAnswered.includes(userID))
                        return <FormOverview {...formOverview} key={i} userID={userID}/>
                } 
            ) : ""}
            </section>
        </main>
    );
}

export default MainContent