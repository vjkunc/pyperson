import './Header.scss';
import avatar from './avatar.png';
import { useNavigate } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux'
import type { RootState } from '../../store/store'
import { removeToken } from '../../store/userSlice'

function Header () {
    const userEmail = useSelector((state: RootState) => state.user.email)
    const dispatch = useDispatch()
    const navigate = useNavigate()
    const logoutUser = () => {
        dispatch(removeToken())
        navigate("/login");
    }
    return (
        <header className='header'>
            <h1 onClick={() => navigate('/')} className="cursor-pointer">Pyperson</h1>
            <input style={{display: "none"}} type="text" placeholder="Vyhledat..."/>

            <article className="dropdown">
                <section className="account">
                    <div>
                        <img src={avatar} alt="avatar-logo"/>
                    </div>
                    <article>
                        <p>{userEmail}</p>
                        <button onClick={logoutUser}>Odhlásit se</button>
                    </article>
                </section>
                <div className="dropdown-content">
                    <a href="/changepassword">Změnit heslo</a>
                </div>
            </article>
        </header>
    );
}

export default Header;