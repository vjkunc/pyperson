import './SurveysByUser.scss';
import FormOverviewByUser from './FormOverviewByUser';
import Navbar from './mainContent/Navbar';
import axios from 'axios';
import { useSelector, useDispatch } from 'react-redux';
import { changeToken } from '../../store/userSlice';
import type { RootState } from '../../store/store';
import { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

function SurveysByUser (props: any) {
    const navigate = useNavigate();
    const [formOverviews, setformOverviews] = useState<any[]>([])
    const authorId = useSelector((state: RootState) => state.user.id);
    const userToken = useSelector((state: RootState) => state.user.token);
    const dispatch = useDispatch();
    const surveys: object[] = [];
    const surveyIds: string[] = [];

    useEffect(() => {
        axios.get('/allForms', {
            headers: {
                Authorization: 'Bearer ' + userToken
            }
        }).then((response: any) => {
            const res = response.data;
            res.access_token && dispatch(changeToken(res.access_token));
            let fOverviews = res.forms;
            fOverviews.map((form: any) => {
                if (form.author == authorId && form.usersAnswered.includes(props.userId) && !surveyIds.includes(form._id)) {
                    surveyIds.push(form._id);
                    surveys.push(form);
                }
            });
            setformOverviews(surveys);
        }).catch((err: any) => {
            if (err.response) {
                console.error(err.response);
                if(err.response.status === 401){
                    dispatch(changeToken(""))
                    navigate('/');
                }
            }
        })
    }, []);

    return (
        <main className='surveysByUser'>
            <Navbar/>
            <section className="list">
                {formOverviews.map((form: any, i: any) => {
                    return <FormOverviewByUser {...form} key={i}/>
                })}
            </section>
        </main>
    );
}

export default SurveysByUser