import './MainContentAdmin.scss';
import FormOverviewAdmin from './mainContent/FormOverviewAdmin';
import Navbar from './mainContent/Navbar';
import axios from 'axios';
import { useSelector, useDispatch } from 'react-redux'
import { changeToken } from '../../store/userSlice'
import type { RootState } from '../../store/store'
import { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

function MainContent () {
    const [formOverviews, setformOverviews] = useState<any[]>([])
    const userID = useSelector((state: RootState) => state.user.id)
    const userToken = useSelector((state: RootState) => state.user.token)
    const dispatch = useDispatch()
    const navigate = useNavigate()
    useEffect(() => { 
        axios.get('/forms/' + userID
        , { 
            headers: {
                Authorization: 'Bearer ' + userToken
                }
            }
        )
        .then(response => {
            const res = response.data
            res.access_token && dispatch(changeToken(res.access_token));
            let fOverviews = res.forms
            for (let formOverview of fOverviews){
                formOverview.completed = formOverview.usersAnswered.length
                formOverview.questionsCount = formOverview.questions.length
            }
            setformOverviews(fOverviews)
        }).catch(err => {
            if (err.response) {
            console.error(err.response);
            if(err.response.status === 401){
                dispatch(changeToken(""))
                navigate('/login');
            }
            }
        })
    }, []);

    return (
        <main className='mainContent'>
            <Navbar/>
            <section>
            {formOverviews ? 
                formOverviews.map((formOverview: any, index: number) =>
                    <FormOverviewAdmin key={index} {...formOverview}/>
            ) : ""}
            </section>
        </main>
    );
}

export default MainContent