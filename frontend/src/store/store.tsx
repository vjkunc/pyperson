import { configureStore, combineReducers } from '@reduxjs/toolkit'
import { persistReducer, persistStore, REGISTER } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import userReducer from './userSlice'

const authPersistConfig = {
  key: 'user',
  storage
}

const rootReducers = combineReducers({ 
  user: persistReducer(authPersistConfig, userReducer)
})


export const store = configureStore({
  reducer: rootReducers,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: false
    })
  })


// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch

export const persistedStore = persistStore(store);