import { createSlice } from '@reduxjs/toolkit'

export const userSlice = createSlice({
  name: 'user',
  initialState: {
    email: "",
    id: "1234",
    role: 2,
    token: ""
  },
  reducers: {
    changeID: (state, action) => {
      state.id = action.payload
    },
    changeEmail: (state, action) => {
      state.email = action.payload
    },
    changeRole: (state, action) => {
      state.role = action.payload
    },
    changeToken: (state, action) => {
      state.token = action.payload
    },
    removeToken: (state) => {
      state.token = ""
    },
  },
})

export const {  
                changeID, 
                changeRole, 
                changeEmail, 
                changeToken, removeToken 

              } = userSlice.actions

export default userSlice.reducer