from enum import IntEnum

class ROLES(IntEnum):
    ADMIN = 1
    BASIC_USER = 2

TYPE_OF_QUESTIONS = [
    "simple",
    "select"
]