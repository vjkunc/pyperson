from flask_mail import Mail, Message
from flask import Flask


class EmailService:
    @classmethod
    def initialize(cls, app):
        cls.mail = Mail(app)

    @classmethod
    def sendForgotPassword(cls, email, token):
        msg = Message()
        msg.recipients = [email]
        msg.subject = "Obnova hesla"
        msg.html = """
            Nové heslo si můžete nastavit na: http://localhost:3000/newpassword/"""+token+""" <br>
            Pokud jste o změnu hesla nežádali můžete tento e-mail ignorovat.
        """
        cls.mail.send(msg)

    @classmethod
    def sendPasswordChanged(cls, email):
        msg = Message()
        msg.recipients = [email]
        msg.subject = "Nové heslo"
        msg.html = """
            Vaše heslo bylo změněno, pokud jste o změnu hesla nežádali doporučujeme vás si co nejdříve heslo změnit.
        """
        cls.mail.send(msg)
