from .DatabaseService import DatabaseService
import GlobalConstants


class AuthorizationService:
    @classmethod
    def isUserAdmin(self, email: str):
        loadedUser = DatabaseService.loadUser({"email": email})
        if (loadedUser['role'] == GlobalConstants.ROLES.ADMIN):
            return True
        else: return False