# TODO: Add error handling and ?maybe make some defense?
from Models.UserModel import User
from Services import db

################### !!! THIS SHOULD NOT BE USED !!! ###################
class TableService:
    ## GET ALL USERS
    def getUsers():
        users = []
        for user in db.session.query(User).all():
            users.append(user.__dict__)
        return users
    
    ## GET ALL USERS BY IDS
    def getUsersByIds(userIds):
        users = []
        for user in db.session.query(User).filter(User.id.in_(userIds)):
            users.append(user.__dict__)
        return users

    ## GET USER BY EMAIL
    def getUser(email):
        user = User.query.filter_by(email = email).first()
        return user

    ## CREATE USER
    def createUser(user):
        db.session.add(user)
        db.session.commit()
        return True

    ## UPDATE USER BY ID
    def updateUser(id, body):
        db.session.query(User).filter_by(id=id).update(
        dict(email = body['email'], username = body['username'], password = body['password'], role = body['role'])
        )
        db.session.commit()
        return True

    ## DELETE USER BY ID
    def deleteUser(id):
        db.session.query(User).filter_by(id=id).delete()
        db.session.commit()
        return True

    ## CHECK IF USER EXISTS BY EMAIL
    def checkIfUserExists(email):
        if (User.query.filter_by(email=email).first()):
            return True
        else:
            return False
