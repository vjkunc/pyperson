import pymongo
from Models.Schemas.FormSchema import formSchema, formsSchema
from Models.Schemas.AnswerSchema import answerSchema, answersSchema
from Models.Schemas.UserSchema import userSchema, usersSchema, usersLimitedSchema
import datetime

class DatabaseService:
    @classmethod
    def initialize(cls):
        client = pymongo.MongoClient("mongodb://pyperson-admin:KiwiPernik-569@mongo-db:27017")
        cls.database = client.pyperson

#### USERS ####
    @classmethod
    def saveUser(cls, data):
        user = userSchema.dump(data)
        cls.database.users.insert_one(user)

    @classmethod
    def loadUser(cls, query):
        user = cls.database.users.find_one(query)
        if user == None:
            return None
        return userSchema.load(user)
    
    @classmethod
    def loadUsers(cls, query):
        users = cls.database.users.find(query)
        if users == None:
            return None
        return usersSchema.load(users)
    
    @classmethod
    def updateUser(cls, query, key, value):
        cls.database.users.update_one(query, {"$set": {key: value}})

    @classmethod
    def deleteUser(cls, query):
        # "Delete" means set email and password to "". Users nick is displayed to admin
        cls.database.users.update_one(query, {"$set": {"password": "", "email": ""}})  
    
    @classmethod
    def addToken(cls, query, token):
        timeNow = datetime.datetime.utcnow()
        valitTime = datetime.timedelta(minutes=15)
        validUntil = (timeNow + valitTime).isoformat()
        cls.database.users.update_one(query, {'$set' : {'token':token, 'validUntil': validUntil}})

    @classmethod
    def deleteToken(cls, token):
        cls.database.users.update_one({'token':token}, {'$set' : {'token': "", 'validUntil':""}})
    
    @classmethod
    def changePassword(cls, token, password):
        cls.database.users.update_one({"token":token}, {'$set' : {'password':password}})
    
#### FORMS ####
    @classmethod
    def saveForm(cls, data):
        form = formSchema.dump(data)
        cls.database.forms.insert_one(form)

    @classmethod
    def loadForm(cls, query):
        form = cls.database.forms.find_one(query)
        if form == None:
            return None
        return formSchema.load(form)
    
    @classmethod
    def loadForms(cls, query):
        forms = cls.database.forms.find(query)
        if forms == None:
            return None
        return formsSchema.load(forms)
    
    @classmethod
    def loadLastNForms(cls, count: int):
        # 1 - ascending, -1 - descending
        forms = cls.database.forms.find().sort("tsCreated", -1).limit(count)
        if forms == None:
            return None
        return formsSchema.load(forms)
    
    @classmethod
    def updateForm(cls, query, key, value):
        cls.database.forms.update_one(query, {"$set": {key: value}})

    @classmethod
    def deleteForm(cls, query):
        cls.database.forms.delete_one(query)

#### ANSWERS ####
    @classmethod
    def saveAnswer(cls, data):
        answer = answerSchema.dump(data)
        cls.database.answers.insert_one(answer)

    @classmethod
    def loadAnswer(cls, query):
        answer = cls.database.answers.find_one(query)
        if answer == None:
            return None
        return answerSchema.load(answer)
    
    @classmethod
    def deleteAnswers(cls, query):
        cls.database.answers.delete_many(query)
    
    @classmethod
    def loadAnswers(cls, query):
        answers = cls.database.answers.find(query)
        if answers == None:
            return None
        return answersSchema.load(answers)




