#### NÁZNAK MOŽNÉ KONFIGURACE NA PRODUKCI/DEVELOPU/TESTU etc.. ####

import os
basedir = os.path.abspath(os.path.dirname(__file__))

class Config(object):
    DEBUG = False
    TESTING = False
    CSRF_ENABLED = True
    SECRET_KEY = 'this-really-needs-to-be-changed'
    SQLALCHEMY_DATABASE_URI = os.environ['postgresql://pyperson-admin:KiwiPernik-569@postgres-db:5432/Pyperson']

class ProductionConfig(Config):
    DEBUG = False


class DevelopmentConfig(Config):
    DEVELOPMENT = True
    DEBUG = True


class TestingConfig(Config):
    TESTING = True