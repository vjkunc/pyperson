from flask import Blueprint, request, jsonify
from Services.EmailService import EmailService
from Services.DatabaseService import DatabaseService
import secrets
import string
import hashlib

emailEndpoint = Blueprint("emailEndpoint", __name__)

@emailEndpoint.route('/forgotPassword', methods=['POST'])
def ForgotPassword():
    try:        
        body = request.get_json()
        email = body['email']
        # check if email exist
        loadedUser = DatabaseService.loadUser({"email": email})
        if not loadedUser:
            return jsonify(errorMessage = "User not found!"), 404 
        # generate token
        alphabet = string.ascii_letters + string.digits
        token = ''.join(secrets.choice(alphabet) for i in range(64))
        tokenHashed = hashlib.sha256(token.encode()).hexdigest()
        loadedUser = DatabaseService.loadUser({"token": tokenHashed})
        while loadedUser:
            token = ''.join(secrets.choice(alphabet) for i in range(64))
            tokenHashed = hashlib.sha256(token.encode()).hexdigest()
            loadedUser = DatabaseService.loadUser({"token": tokenHashed})

        DatabaseService.addToken({"email": email}, tokenHashed)
        # send mail
        EmailService.sendForgotPassword(email, token)
        return jsonify("Email sended!"), 200
    except Exception as e:
        print(e)
        return jsonify(errorMessage = "Something went wrong!"), 400
    
