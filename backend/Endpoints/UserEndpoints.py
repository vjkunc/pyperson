import json
from flask import Blueprint, request, jsonify
from Models.UserModel import User
from Models.Schemas.UserSchema import userLimitedSchema, usersLimitedSchema
from Services.DatabaseService import DatabaseService
from Services.AuthorizationService import AuthorizationService
from Services.EmailService import EmailService
from werkzeug.security import generate_password_hash, check_password_hash
from GlobalConstants import ROLES
from datetime import datetime, timedelta, timezone
from flask_jwt_extended import create_access_token,get_jwt,get_jwt_identity, \
                               unset_jwt_cookies, jwt_required
import hashlib

userEndpoint = Blueprint('userEndpoint', __name__)

## Endpoints only for admin
@userEndpoint.route('/users', methods=['POST'])
@jwt_required()
def GetUsersByIds():
    try:
        # Authorization
        claims = get_jwt()
        isUserAdmin: bool = AuthorizationService.isUserAdmin(email = claims['sub'])
        if not (isUserAdmin): 
            return jsonify("User is not admin!"), 401
        
        body = request.get_json()
        userIds = body['userIds']
        print(type(userIds), flush=True)
        loadedUsers = DatabaseService.loadUsers({"_id": { "$in": userIds }})
        if not loadedUsers:
            return jsonify(errorMessage = "Users has not been found!"), 404 # No content result
        users = []
        for loadedUser in loadedUsers:
            users.append(User(loadedUser['email'], 
                              loadedUser['nickname'], 
                              loadedUser['password'], 
                              loadedUser['role'],
                              loadedUser['validUntil'],
                              loadedUser['token'],
                              loadedUser['_id'],
                              loadedUser['tsCreated'] 
                            ))
            # TODO: This schema should be changed to limited for ADMIN -> it should not contains email
        return jsonify(users = usersLimitedSchema.dump(loadedUsers)), 200
    except Exception as e:
        print(str(e))
        return jsonify(errorMessage = "Something went wrong Look at the console!"), 400 # Bad request

@userEndpoint.route('/newPassword', methods=['POST'])
@jwt_required()
def UpdateUser():
    try:
        # Obtain variables
        body = request.get_json()
        email = body['email']
        password = body['password']
        newPassword = generate_password_hash(body['newPassword'], method='sha256')

        # Defense
        loadedUser = DatabaseService.loadUser({"email": email})
        if not loadedUser:
            return jsonify(errorMessage = "User has not been found!"), 404 # No content result
        elif not (check_password_hash(loadedUser['password'], password)):
            return jsonify(errorMessage = "Password is not correct"), 401  # Unauthorized result
        
        # Update
        DatabaseService.updateUser({"email": email}, "password", newPassword)
        return jsonify(message = "User succesfully updated!"), 200
    except Exception as e:
        print(str(e))
        return jsonify(errorMessage = "Something went wrong Look at the console!"), 400 # Bad request
    
@userEndpoint.route('/deleteUser', methods=['POST'])
@jwt_required()
def DeleteUser():
    try:
        # Obtain variables
        body = request.get_json()
        email = body['email']
        password = body['password']

        # Defense
        loadedUser = DatabaseService.loadUser({"email": email})
        if not loadedUser:
            return jsonify(errorMessage = "User has not been found!"), 404 # No content result
        elif not (check_password_hash(loadedUser['password'], password)):
            return jsonify(errorMessage = "Password is not correct"), 401  # Unauthorized result
        
        # Delete
        DatabaseService.deleteUser({"email": email})
        return jsonify(message = "User succesfully deleted!"), 200
    except Exception as e:
        print(str(e))
        return jsonify(errorMessage = "Something went wrong Look at the console!"), 400 # Bad request
    
## Rest of endpoints
@userEndpoint.route('/signup', methods=['POST'])
def signUp():
    try:
        # Obtain variables from request
        body = request.get_json()
        email = body['email']
        password = generate_password_hash(body['password'], method='sha256')
        role = body['role']
        nickname = body['nickname']
        loadedUser = DatabaseService.loadUser({"email": email}) # If this returns a user, then email already exists in DB
        if loadedUser:
            return jsonify(errorMessage = f"The user with email '{email}' already exists!"), 400 # Bad request result 
        elif DatabaseService.loadUser({"nickname": nickname}):
            return jsonify(errorMessage = f"The user with nickname '{nickname}' already exists!"), 400 # Bad request result 
        if not (role in iter(ROLES)):
            raise ValueError(f"[!THIS IS ENUM!] This role is not declared! Unexpected role: '{role}'")
        
        user = User(email,
                    nickname,
                    password,
                    role)
        
        DatabaseService.saveUser(user)
        return jsonify("The user was successfully created!"), 201 # Created result (success)

    except Exception as e:
        print(str(e))
        return jsonify(errorMessage = "Something went wrong! Look at the console!"), 400 # Bad request

@userEndpoint.route('/login', methods=['POST'])
def login():
    try:
        body = request.get_json()
        email = body['email']
        password = body['password']

        loadedUser = DatabaseService.loadUser({"email": email})
        if not loadedUser:
            return jsonify(errorMessage = "User has not been found!"), 404 # No content result
        elif not (check_password_hash(loadedUser['password'], password)):
            return jsonify(errorMessage = "Password is not correct"), 401  # Unauthorized result
        createdAccessToken = create_access_token(identity=email)
        user = User(loadedUser['email'],
                    loadedUser['nickname'],
                    loadedUser['password'],
                    loadedUser['role'],
                    loadedUser['validUntil'],
                    loadedUser['token'],
                    loadedUser['_id'],
                    loadedUser['tsCreated'])
        return jsonify(accessToken = createdAccessToken, data = userLimitedSchema.dump(user)), 200
    except Exception as e:
        print(str(e))
        return jsonify(errorMessage = "Something went wrong Look at the console!"), 400 # Bad request

@userEndpoint.route("/logout", methods=["POST"])
@jwt_required()
def logout():
    response = jsonify({"message": "Logout was successful!"})
    unset_jwt_cookies(response)
    return response, 200

@userEndpoint.after_request
def refresh_expiring_jwts(response):
    try:
        expiredTimestamp = get_jwt()["exp"]
        now = datetime.now(timezone.utc)
        targetTimestamp = datetime.timestamp(now + timedelta(minutes=30))
        if targetTimestamp > expiredTimestamp:
            newAccessToken = create_access_token(identity=get_jwt_identity())
            response = json.dumps({"accessToken": newAccessToken})
        return response
    except (RuntimeError, KeyError):
        # Case where there is not a valid JWT. Just return the original respone
        return response

@userEndpoint.route('/resetPassword', methods=['POST'])
def resetPassword():
    try:
        body = request.get_json()
        token = body['token']
        token = hashlib.sha256(token.encode()).hexdigest()
        password = body['password']
        loadedUser = DatabaseService.loadUser({"token": token})
        if not loadedUser:
            return jsonify(errorMessage = "Token not exist!"), 400 
        timeNow = datetime.utcnow().isoformat()
        if(timeNow > loadedUser['validUntil']):
            return jsonify(errorMessage = "Token expired!"), 400
        password = generate_password_hash(password, method='sha256')
        DatabaseService.changePassword(token, password)
        DatabaseService.deleteToken(token)
        EmailService.sendPasswordChanged(loadedUser['email'])
        return jsonify("Password changed"), 200
    except Exception as e:
        print(str(e))
        return jsonify(errorMessage = "Something went wrong Look at the console!"), 400 # Bad request