from flask import Blueprint, request, jsonify
from Services.DatabaseService import DatabaseService
from Services.AuthorizationService import AuthorizationService
from Models.FormModel import Form
from Models.QuestionModel import Question
from Models.Schemas.FormSchema import formSchema, formsSchema, formsLimitedSchema, \
    formForBasicUserSchema, formsForBasicUserLimitedSchema
from flask_jwt_extended import jwt_required, get_jwt


formEndpoint = Blueprint("formEndpoint", __name__)

# Endpoints only for admin
@formEndpoint.route('/createForm', methods=['POST'])
@jwt_required()
def CreateForm():
    try:       
        # Authorization
        claims = get_jwt()
        isUserAdmin: bool = AuthorizationService.isUserAdmin(email = claims['sub'])
        if not (isUserAdmin): 
            return jsonify("User is not admin!"), 401
        
        # Obtain variables from request
        # TODO: Validation of variables
        body = request.get_json()
        author = body['author']
        lengthInMinutes = body['lengthInMinutes']
        description = body['description']
        recommended = body['recommended']
        title = body['title']
        questions = body['questions']
        
        # Create form
        questionInstances = []
        for question in questions:
             questionInstances.append(Question(question['question'],
                                               question['required'], 
                                               question['typeOfAnswer'], 
                                               question['options'], 
                                               question['description']))           
        form = Form(author, lengthInMinutes, description, recommended, title, questionInstances)

        # Save form to database
        DatabaseService.saveForm(form)

        return jsonify("Form was successfully created!"), 200
    except Exception as e:
        print(e, flush=True)
        return jsonify(errorMessage = "Something went wrong!"), 400
    
@formEndpoint.route('/setRecommended/<formId>', methods=['POST'])
@jwt_required()
def SetRecommended(formId):
    try:       
        # Authorization
        claims = get_jwt()
        isUserAdmin: bool = AuthorizationService.isUserAdmin(email = claims['sub'])
        if not (isUserAdmin): 
            return jsonify("User is not admin!"), 401
    
        loadedForm = DatabaseService.loadForm({"_id": formId})
        
        # Change recommended
        if (loadedForm['recommended'] is True):
            recommended = False
        else: 
            recommended = True
        
        # Update form in database
        DatabaseService.updateForm({"_id": formId}, "recommended", recommended)
        return jsonify("Form was successfully updated!"), 200
    except Exception as e:
        print(e, flush=True)
        return jsonify(errorMessage = "Something went wrong!"), 400
    
@formEndpoint.route('/deleteForm/<formId>', methods=['POST'])
@jwt_required()
def DeleteForm(formId):
    try:       
        # Authorization
        claims = get_jwt()
        isUserAdmin: bool = AuthorizationService.isUserAdmin(email = claims['sub'])
        if not (isUserAdmin): 
            return jsonify("User is not admin!"), 401
    
        loadedForm = DatabaseService.loadForm({"_id": formId})
        if not (loadedForm):
            return jsonify("Form was not found!"), 404
        
        # Delete form
        DatabaseService.deleteForm({"_id": formId})
        
        # Delete all answers for this form
        DatabaseService.deleteAnswers({"formId": formId})

        return jsonify("Form was successfully deleted!"), 200
    except Exception as e:
        print(e, flush=True)
        return jsonify(errorMessage = "Something went wrong!"), 400
    
@formEndpoint.route('/forms/<authorId>', methods=['GET'])
@jwt_required()
def GetAllFormsWithLimitedSchemaByUserId(authorId):
    try:
        # Authorization
        claims = get_jwt()
        isUserAdmin: bool = AuthorizationService.isUserAdmin(email = claims['sub'])
        if not (isUserAdmin): 
            return jsonify("User is not admin!"), 401
        
        loadedForms = DatabaseService.loadForms({"author": authorId})
        forms = []
        for loadedForm in loadedForms:
            forms.append(Form(loadedForm['author'], 
                                loadedForm['lengthInMinutes'], 
                                loadedForm['description'], 
                                loadedForm['recommended'],
                                loadedForm['title'],
                                loadedForm['questions'],
                                loadedForm['usersAnswered'],
                                loadedForm['_id'],
                                loadedForm['tsCreated'] 
                            ))
        return jsonify(message = "The forms were succesfully obtained!", forms = formsSchema.dump(forms)), 200
    except Exception as e:
        print(e)
        return jsonify(errorMessage = "Something went wrong!"), 400
    
# Endpoints that returns different schema due to user role
@formEndpoint.route('/form/<formId>', methods=['GET'])
@jwt_required()
def GetFormById(formId):
    try:
        # Authorization
        claims = get_jwt()
        isUserAdmin: bool = AuthorizationService.isUserAdmin(email = claims['sub'])

        loadedForm = DatabaseService.loadForm({"_id": formId})
        if loadedForm:
            form = Form(loadedForm['author'], 
                        loadedForm['lengthInMinutes'], 
                        loadedForm['description'], 
                        loadedForm['recommended'],
                        loadedForm['title'],
                        loadedForm['questions'],
                        loadedForm['usersAnswered'],
                        loadedForm['_id'],
                        loadedForm['tsCreated'] 
                        )
            if not (isUserAdmin): return jsonify(message = "The form was succesfully obtained for basic user!", form = formForBasicUserSchema.dump(form)), 200
            return jsonify(message = "The form was succesfully obtained for admin!", form = formSchema.dump(form)), 200
    except Exception as e:
        print(e)
        return jsonify(errorMessage = "Something went wrong!"), 400
    
@formEndpoint.route('/allForms', methods=['GET'])
@jwt_required()
def GetAllFormsWithLimitedSchema():
    try:
        # Authorization
        claims = get_jwt()
        isUserAdmin: bool = AuthorizationService.isUserAdmin(email = claims['sub'])

        loadedForms = DatabaseService.loadForms({})
        if loadedForms:
            forms = []
            for loadedForm in loadedForms:
                forms.append(Form(loadedForm['author'], 
                                  loadedForm['lengthInMinutes'], 
                                  loadedForm['description'], 
                                  loadedForm['recommended'],
                                  loadedForm['title'],
                                  loadedForm['questions'],
                                  loadedForm['usersAnswered'],
                                  loadedForm['_id'],
                                  loadedForm['tsCreated'] 
                                ))
            if not (isUserAdmin): return jsonify(message = "The forms were succesfully obtained for basic user!", forms = formsForBasicUserLimitedSchema.dump(forms)), 200
            return jsonify(message = "The forms were succesfully obtained for admin!", forms = formsLimitedSchema.dump(forms)), 200
    except Exception as e:
        print(e, flush=True)
        return jsonify(errorMessage = "Something went wrong!"), 400
    
@formEndpoint.route('/forms', methods=['GET'])
@jwt_required
def GetFormsByListOfIds():
    try:
        # Authorization
        claims = get_jwt()
        isUserAdmin: bool = AuthorizationService.isUserAdmin(email = claims['sub'])

        body = request.get_json()
        formIds = body['formIds']
        loadedForms = DatabaseService.loadForms({"_id": { "$in": formIds }})
        if loadedForms:
            forms = []
            for loadedForm in loadedForms:
                forms.append(Form(loadedForm['author'], 
                                  loadedForm['lengthInMinutes'], 
                                  loadedForm['description'], 
                                  loadedForm['recommended'],
                                  loadedForm['title'],
                                  loadedForm['questions'],
                                  loadedForm['usersAnswered'],
                                  loadedForm['_id'],
                                  loadedForm['tsCreated'] 
                                ))
            if not (isUserAdmin): return jsonify(message = "The forms were succesfully obtained for basic user!", forms = formsForBasicUserLimitedSchema.dump(forms)), 200
            return jsonify(message = "The forms were succesfully obtained for admin!", forms = formsLimitedSchema.dump(forms)), 200
    except Exception as e:
        print(e, flush=True)
        return jsonify(errorMessage = "Something went wrong!"), 400
    
# Endpoints that returns just schema for basic user
@formEndpoint.route('/lastThreeForms', methods=['GET'])
def GetLastThreeFormsWithLimitedSchema():
    try:
        loadedForms = DatabaseService.loadLastNForms(3)
        if loadedForms:
            forms = []
            for loadedForm in loadedForms:
                forms.append(Form(loadedForm['author'], 
                                  loadedForm['lengthInMinutes'], 
                                  loadedForm['description'], 
                                  loadedForm['recommended'],
                                  loadedForm['title'],
                                  loadedForm['questions'],
                                  loadedForm['usersAnswered'],
                                  loadedForm['_id'],
                                  loadedForm['tsCreated'] 
                                ))
            return jsonify(message = "The forms were succesfully obtained!", forms = formsForBasicUserLimitedSchema.dump(forms)), 200
    except Exception as e:
        print(e, flush=True)
        return jsonify(errorMessage = "Something went wrong!"), 400