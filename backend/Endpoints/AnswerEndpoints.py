from flask import Blueprint, request, jsonify
from Services.DatabaseService import DatabaseService
from Services.AuthorizationService import AuthorizationService
from Models.Schemas.AnswerSchema import answerSchema, answersSchema, answerForBasicUserSchema, answersForBasicUserSchema
from Models.Schemas.UserSchema import userSchema
from Models.AnswerModel import Answer
from Models.FormModel import Form
from flask_jwt_extended import jwt_required, get_jwt


answerEndpoint = Blueprint("answerEndpoint", __name__)

@answerEndpoint.route('/createAnswer', methods=['POST'])
@jwt_required()
def CreateAnswer():
    try:
        # Obtain variables from request
        # TODO: Validation of variables
        body = request.get_json()
        answers = body['answers']
        userAnsweredId = body['userAnsweredId']
        formId = body['formId']

        loadedAnswer = DatabaseService.loadAnswer({"$and": [{"userAnsweredId": userAnsweredId}, {"formId": formId} ]})
        if loadedAnswer:
            return jsonify(errorMessage = "This form is already filled up!"), 400
        
        # Create answer        
        answer = Answer(answers, userAnsweredId, formId)
            
        # Save answer to database
        DatabaseService.saveAnswer(answer)

        # Add user answered id to form
        loadedForm = DatabaseService.loadForm({"_id": formId})
        if loadedForm:
            form = Form(loadedForm['author'], 
                        loadedForm['lengthInMinutes'], 
                        loadedForm['description'], 
                        loadedForm['recommended'],
                        loadedForm['title'],
                        loadedForm['questions'],
                        loadedForm['usersAnswered'],
                        loadedForm['_id'],
                        loadedForm['tsCreated'] 
                        )
            form.usersAnswered.append(userAnsweredId)
            DatabaseService.updateForm({"_id": formId}, "usersAnswered", form.usersAnswered)
            return jsonify("Answer was successfully created!"), 200
    except Exception as e:
        print(e)
        return jsonify(errorMessage = "Something went wrong!"), 400
    
@answerEndpoint.route('/answer/<userId>/<formId>', methods=['GET'])
@jwt_required()
def GetAnswerById(userId, formId):
    try:
        # Authorization
        claims = get_jwt()
        isUserAdmin: bool = AuthorizationService.isUserAdmin(email = claims['sub'])

        loadedAnswer = DatabaseService.loadAnswer({"$and": [{"userAnsweredId": userId}, {"formId": formId} ]})
        if loadedAnswer:
            answer = Answer(loadedAnswer['answers'], 
                            loadedAnswer['userAnsweredId'], 
                            loadedAnswer['formId'], 
                            loadedAnswer['_id'],
                            loadedAnswer['tsCreated'] 
                        )
            if not (isUserAdmin): return jsonify(message = "The answer was succesfully obtained for basic user!", data = {"answers": answer.answers}), 200
            user = userSchema.dump(DatabaseService.loadUser({"_id": answer.userAnsweredId}))
            return jsonify(message = "The answer was succesfully obtained for admin!", data = {"userNickname": user['nickname'], "answers": answer.answers}), 200
    except Exception as e:
        print(e)
        return jsonify(errorMessage = "Something went wrong!"), 400
    

@answerEndpoint.route('/answers/<formId>', methods=['GET'])
@jwt_required()
def GetAnswersByFormId(formId):
    try:
        # Authorization
        claims = get_jwt()
        isUserAdmin: bool = AuthorizationService.isUserAdmin(email = claims['sub'])
        if not (isUserAdmin): 
            return jsonify("User is not admin!"), 401
        
        loadedAnswers = DatabaseService.loadAnswers({"formId": formId})
        dataForExport = []
        if loadedAnswers:
            for loadedAnswer in loadedAnswers:
                answer = Answer(loadedAnswer['answers'], 
                                    loadedAnswer['userAnsweredId'], 
                                    loadedAnswer['formId'], 
                                    loadedAnswer['_id'],
                                    loadedAnswer['tsCreated'])
                user = userSchema.dump(DatabaseService.loadUser({"_id": answer.userAnsweredId}))
                data = {
                    "userNickname": user['nickname'],
                    "answers": answer.answers
                }
                dataForExport.append(data)
                
        return jsonify(message = "The answers were succesfully obtained for admin!", data = dataForExport), 200
    except Exception as e:
        print(e)
        return jsonify(errorMessage = "Something went wrong!"), 400
    