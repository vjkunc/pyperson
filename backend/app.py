import os
from flask import Flask
from Services import db
from Services.DatabaseService import DatabaseService
from Services.EmailService import EmailService
from Models import ma
from debugger import initialize_flask_server_debugger_if_needed
from flask_jwt_extended import JWTManager
from datetime import timedelta

initialize_flask_server_debugger_if_needed()

# Init app
app = Flask(__name__)
basedir = os.path.abspath(os.path.dirname(__file__))

# Database
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get('DATABASE_URL')
app.config['SQLALCHEMY_DATABASE_MODIFICATIONS'] = False
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

# JWT
app.config["JWT_SECRET_KEY"] = "please-remember-to-change-me"
app.config["JWT_ACCESS_TOKEN_EXPIRES"] = timedelta(hours=1)
jwt = JWTManager(app)

# Init database and marshmallow for db schemas..
db.__init__(app)
ma.__init__(app)
DatabaseService.initialize()


# Mail
app.config['MAIL_SERVER']='smtp.gmail.com'
app.config['MAIL_PORT'] = 465
app.config['MAIL_USERNAME'] = 'pyperson23@gmail.com'
app.config['MAIL_PASSWORD'] = 'yqhisljujjbuycnx'
app.config['MAIL_DEFAULT_SENDER'] = 'pyperson23@gmail.com'
app.config['MAIL_USE_TLS'] = False
app.config['MAIL_USE_SSL'] = True
EmailService.initialize(app)


# Endpoints registration
from Endpoints.UserEndpoints import userEndpoint
app.register_blueprint(userEndpoint)
from Endpoints.FormEndpoints import formEndpoint
app.register_blueprint(formEndpoint)
from Endpoints.AnswerEndpoints import answerEndpoint
app.register_blueprint(answerEndpoint)

from Endpoints.EmailEndpoints import emailEndpoint
app.register_blueprint(emailEndpoint)

# Thanks to SQLAlchemy synchronize with the Postgres database. This will create our database table automatically.
db.create_all()

if __name__ == '__main__':
   app.run(host='0.0.0.0',port = '5001', debug = True)