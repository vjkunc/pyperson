from marshmallow import Schema, fields

class AnswerSchema(Schema):
    answers = fields.List(fields.String(allow_none=True))
    userAnsweredId = fields.String()
    formId = fields.String()
    _id = fields.String()
    tsCreated = fields.String()

class AnswerForBasicUserSchema(Schema):
    answers = fields.List(fields.String(allow_none=True))
    formId = fields.String()
    _id = fields.String()
    tsCreated = fields.String()

# For admin
answerSchema = AnswerSchema()
answersSchema = AnswerSchema(many=True)

# For basic user
answerForBasicUserSchema = AnswerForBasicUserSchema()
answersForBasicUserSchema = AnswerForBasicUserSchema(many=True)