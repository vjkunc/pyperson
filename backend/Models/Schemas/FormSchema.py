from marshmallow import Schema, fields
from .QuestionSchema import QuestionSchema

class FormSchema(Schema):
    author = fields.String()
    lengthInMinutes = fields.Integer()
    description = fields.String()
    recommended = fields.Boolean()
    title = fields.String()
    questions = fields.List(fields.Nested(QuestionSchema))
    usersAnswered = fields.List(fields.String())
    _id = fields.String()
    tsCreated = fields.String()

class FormForBasicUserSchema(Schema):
    lengthInMinutes = fields.Integer()
    description = fields.String()
    recommended = fields.Boolean()
    title = fields.String()
    questions = fields.List(fields.Nested(QuestionSchema))
    usersAnswered = fields.List(fields.String())
    _id = fields.String()
    tsCreated = fields.String()

class FormLimitedSchema(Schema):
    lengthInMinutes = fields.Integer()
    description = fields.String()
    recommended = fields.Boolean()
    title = fields.String()
    questions = fields.List(fields.Nested(QuestionSchema))
    usersAnswered = fields.List(fields.String())
    _id = fields.String()
    tsCreated = fields.String()
    author = fields.String()

class FormForBasicUserLimitedSchema(Schema):
    lengthInMinutes = fields.Integer()
    description = fields.String()
    recommended = fields.Boolean()
    title = fields.String()
    questions = fields.List(fields.Nested(QuestionSchema))
    usersAnswered = fields.List(fields.String())
    _id = fields.String()
    tsCreated = fields.String()

# For admin
formSchema = FormSchema()
formsSchema = FormSchema(many=True)
formsLimitedSchema = FormLimitedSchema(many=True)

# For basic user
formForBasicUserSchema = FormForBasicUserSchema()
formsForBasicUserSchema = FormForBasicUserSchema(many=True)
formsForBasicUserLimitedSchema = FormForBasicUserLimitedSchema(many=True)