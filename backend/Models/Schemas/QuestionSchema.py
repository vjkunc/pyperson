from marshmallow import Schema, fields

class QuestionSchema(Schema):
    question = fields.String()
    required = fields.Boolean()
    typeOfAnswer = fields.String()
    options = fields.List(fields.String(), allow_none=True)
    description = fields.String()

questionSchema = QuestionSchema()