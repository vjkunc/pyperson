from marshmallow import Schema, fields

class UserSchema(Schema):
    _id = fields.String()
    email = fields.String()
    nickname = fields.String()
    password = fields.String()
    role = fields.Integer()
    tsCreated = fields.String()
    validUntil = fields.String()
    token = fields.String()

class UserLimitedSchema(Schema):
    _id = fields.String()
    email = fields.String()
    nickname = fields.String()
    role = fields.Integer()
    tsCreated = fields.String()
    validUntil = fields.String()
    token = fields.String()

userSchema = UserSchema()
usersSchema = UserSchema(many=True)
userLimitedSchema = UserLimitedSchema()
usersLimitedSchema = UserLimitedSchema(many=True)