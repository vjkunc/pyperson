from uuid import uuid4
import datetime

class Answer():
    def __init__( self,
                  answers:list,
                  userAnsweredId:str,
                  formId:str,
                  _id:str = None,
                  tsCreated:str = None,
                ):
        self._id = uuid4() if _id is None else _id
        self.tsCreated = str(datetime.datetime.utcnow().isoformat()) if tsCreated is None else tsCreated
        self.answers = answers
        self.userAnsweredId = userAnsweredId
        self.formId = formId
