from uuid import uuid4
import datetime

class User():
   def __init__(self,
                email:str,
                nickname:str,
                password:str,
                role:int,
                validUntil:str = None,
                token:str = None,
                _id:str = None,
                tsCreated:str = None,):
      self._id = uuid4() if _id is None else _id
      self.tsCreated = str(datetime.datetime.utcnow().isoformat()) if tsCreated is None else tsCreated
      self.validUntil = "" if validUntil is None else validUntil
      self.token = "" if token is None else token
      self.email = email
      self.nickname = nickname 
      self.password = password
      self.role = role