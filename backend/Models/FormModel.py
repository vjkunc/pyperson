from uuid import uuid4
import datetime

class Form():
    def __init__( self,
                  author:str,
                  lengthInMinutes:int,
                  description:str,
                  recommended:bool,
                  title:str,
                  questions:list,
                  usersAnswered:list = [],
                  _id:str = None,
                  tsCreated:str = None,
                ):
        self._id = uuid4() if _id is None else _id
        self.author = author
        self.tsCreated = str(datetime.datetime.utcnow().isoformat()) if tsCreated is None else tsCreated
        self.lengthInMinutes = lengthInMinutes
        self.description = description
        self.recommended = recommended
        self.title = title
        self.questions = questions
        self.usersAnswered = usersAnswered
