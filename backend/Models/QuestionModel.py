class Question():
    def __init__(self, question:str, required:bool, typeOfAnswer:str, options:list, description:str ):
        self.question = question
        self.required = required
        self.typeOfAnswer = typeOfAnswer
        self.options = options
        self.description = description